﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class ScoresController : MonoBehaviour {
	// Penalty settings
	public int _wildcardPenalty;
	public int _normalCardPenalty;
	public int _combinationLevelPenalty;

	private Lang _lang;

	public GameObject _continueButton;
	public GameObject _backButton;
	public GameObject _explenation;

	// UI Elements
	public GameObject[] _combinationNames;
	public GameObject[] _combinationValues;
	public GameObject[] _rankingNames;
	public GameObject[] _rankingIncrements;
	public GameObject[] _rankingScores;
	public GameObject[] _rankingButtons;
	public GameObject[] _rankingRanks;

	private List<Player> _players;
	private List<BestCombinationContainer> _orderedBestCombinations;
	private string _winnerExplenation;
	private List<IncrementContainer> _orderedIncrements;

	// Use this for initialization
	void Start () {
		_orderedIncrements = new List<IncrementContainer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void init(Lang lang) {
		_lang = lang;
	}

	public void setPlayers(List<Player> players) {
		_players = players;
	}

	/**
	 * Show the best combination for each player
	 */
	public void showBestCombinations() {
		List<BestCombinationContainer> bestCombinations = new List<BestCombinationContainer>();

		// Zoek voor iedere speler zijn beste combinatie
		foreach(Player player in _players) {
			List<Combination> combinations = player.getHand().getPublicCards();

			if(combinations.Count > 0) {
				CombinationComparer comparer = new CombinationComparer();
				combinations.Sort(comparer);

				Combination bestCombination = combinations[combinations.Count - 1];
				//Debug.Log(bestCombination.toRichText());

				// Maak voor iedere speler een combinationpair
				bestCombinations.Add(new BestCombinationContainer(bestCombination, player));
			} else {
				bestCombinations.Add(new BestCombinationContainer(null, player));
			}
		}

		// Sorter combination pair --> oplopende combinatie, De laatste is de beste
		BestCombinationContainerComparer bestCombinationComparer = new BestCombinationContainerComparer();
		bestCombinations.Sort(bestCombinationComparer);
		_orderedBestCombinations = bestCombinations;

		updateCombinationsUI();
	}

	private void updateCombinationsUI() {
		Dictionary<int, string> ranks = RankLoader.getRanks();
		hideAll(_combinationNames);
		hideAll(_combinationValues);
		hideAll(_rankingRanks);

		for(int i = 0; i < _players.Count; i++) {
			BestCombinationContainer container = _orderedBestCombinations[_orderedBestCombinations.Count - 1 - i];
			_combinationNames[i].GetComponent<Text>().text = container.getPlayer().getName();

			Combination combination = container.getCombination();
			int rank = CombinationChecker.calculateValue(combination);

			if(rank == 0) {
				_combinationValues[i].GetComponent<Text>().text = ranks[0];
				_rankingRanks[i].GetComponent<Text>().text = "";
			} else {
				_combinationValues[i].GetComponent<Text>().text = combination.toRichText();
				_rankingRanks[i].GetComponent<Text>().text = ranks[rank];
			}
			
			_combinationNames[i].SetActive(true);
			_combinationValues[i].SetActive(true);
			_rankingRanks[i].SetActive(true);
		}
	}

	public void updateScores() {
		_orderedIncrements = new List<IncrementContainer>();
		BestCombinationContainer lastCombinationContainer = _orderedBestCombinations[_orderedBestCombinations.Count - 1];
		int winnerIncrement = 0;
		int winnerCombinationLevel = CombinationChecker.calculateValue(lastCombinationContainer.getCombination());

		Hashtable winnerReplacements = new Hashtable();
		winnerReplacements.Add("winnerCombinationLevel", "" + winnerCombinationLevel);
		string loserEntries = "";

		for(int i = 0; i < _orderedBestCombinations.Count - 1; i++) {
			Hashtable loserReplacements = new Hashtable();
			Hashtable loserEntryReplacements = new Hashtable();
			loserReplacements.Add("winnerCombinationLevel", "" + winnerCombinationLevel);

			Player player = _orderedBestCombinations[i].getPlayer();
			Hand hand = player.getHand();

			int bestCombinationLevel = CombinationChecker.calculateValue(_orderedBestCombinations[i].getCombination());
			int combinationPenalty = (winnerCombinationLevel - bestCombinationLevel) * _combinationLevelPenalty;
			int cardCounter = 0;
			int wildcardCounter = 0;

			loserReplacements.Add("playerCombinationLevel", "" + bestCombinationLevel);
			loserReplacements.Add("combinationLevelPenalty", "" + _combinationLevelPenalty);
			loserReplacements.Add("appliedCombinationLevelPenalty", "" + combinationPenalty);

			foreach (Card card in hand.getPrivateCards()) { 
				if(card.isJoker()) {
					wildcardCounter++;
				} else {
					cardCounter++;
				}
			}

			int cardPenalty = cardCounter * _normalCardPenalty;
			int wildcardPenalty = wildcardCounter * _wildcardPenalty;
			int playerIncrement = -1 * (combinationPenalty + cardPenalty + wildcardPenalty);

			loserReplacements.Add("normalCardPenalty", "" + _normalCardPenalty);
			loserReplacements.Add("numNormalCards", "" + cardCounter);
			loserReplacements.Add("appliedCardPenalty", "" + cardPenalty);
			loserReplacements.Add("wildcardPenalty", "" + _wildcardPenalty);
			loserReplacements.Add("numWildcards", "" + wildcardCounter);
			loserReplacements.Add("appliedWildcardPenalty", "" + wildcardPenalty);
			loserReplacements.Add("playerIncrement", "" + playerIncrement);

			loserEntryReplacements.Add("loser_increment", "" + (-1 * playerIncrement));
			loserEntryReplacements.Add("loser_name", player.getName());

			loserEntries += TemplatingEngine.replace(_lang.getString("loser_entry"), loserEntryReplacements);
			string playerExplenation = TemplatingEngine.replace(_lang.getString("loser_explenation"), loserReplacements);

			_orderedIncrements.Add(new IncrementContainer(playerIncrement, _orderedBestCombinations[i].getPlayer(), playerExplenation));
			_orderedBestCombinations[i].getPlayer().incrementScore(playerIncrement);

			winnerIncrement += -1 * playerIncrement;
		}

		winnerReplacements.Add("winnerIncrement", "" + winnerIncrement);
		winnerReplacements.Add("loser_entries", loserEntries);
		string winnerExplenation = TemplatingEngine.replace(_lang.getString("winner_explenation"), winnerReplacements);

		_orderedIncrements.Add(new IncrementContainer(winnerIncrement, lastCombinationContainer.getPlayer(), winnerExplenation));
		lastCombinationContainer.getPlayer().incrementScore(winnerIncrement);

		IncrementContainerComparer comparer = new IncrementContainerComparer();
		_orderedIncrements.Sort(comparer);

		updateScoresUI();
	}

	private void updateScoresUI() {
		hideAll(_rankingNames);
		hideAll(_rankingIncrements);
		hideAll(_rankingScores);
		hideAll(_rankingButtons);
		hideAll(_combinationNames);

		for(int i = 0; i < _orderedIncrements.Count; i++) {
			IncrementContainer container = _orderedIncrements[_orderedIncrements.Count - i - 1];

			_rankingNames[i].GetComponent<Text>().text = container.getPlayer().getName();
			_rankingIncrements[i].GetComponent<Text>().text = container.getRichIncrement();
			_rankingScores[i].GetComponent<Text>().text = "" + container.getPlayer().getScore();
			_rankingNames[i].SetActive(true);
			_rankingIncrements[i].SetActive(true);
			_rankingScores[i].SetActive(true);
			_rankingButtons[i].SetActive(true);
			_rankingRanks[i].SetActive(true);
		}
	}

	public void showExplenation(int rankID) {
		hideAll(_rankingNames);
		hideAll(_rankingIncrements);
		hideAll(_rankingScores);
		hideAll(_rankingButtons);
		hideAll(_rankingRanks);
		hideAll(_combinationNames);

		_explenation.GetComponent<Text>().text = _orderedIncrements[_orderedIncrements.Count - 1 - rankID].getExplenation();

		_explenation.SetActive(true);
		_continueButton.SetActive(false);
		_backButton.SetActive(true);
	}

	public void stopExplenation() {
		_continueButton.SetActive(true);
		_backButton.SetActive(false);
		_explenation.SetActive(false);

		for(int i = 0; i < _players.Count; i++) {
			_rankingNames[i].SetActive(true);
			_rankingIncrements[i].SetActive(true);
			_rankingScores[i].SetActive(true);
			_rankingButtons[i].SetActive(true);
			_rankingRanks[i].SetActive(true);
		}
	}

	private void hideAll(GameObject[] gameObjectList) {
		foreach(GameObject gameObject in gameObjectList) {
			gameObject.SetActive(false);
		}
	}
}