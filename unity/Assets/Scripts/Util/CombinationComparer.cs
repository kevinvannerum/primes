﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;
	
public class CombinationComparer : IComparer<Combination>  {
	public const int SMALLER 	= -1;
	public const int EQUAL 		= 0;
	public const int LARGER 	= 1;

	/**
	 * -1 --> x < y
	 * 0  --> x = y
	 * 1  --> x > y
	 */
	public int Compare( Combination x, Combination y )  {
        if(x == null) {
            if(y == null) {
                return EQUAL;
            } else {
                return SMALLER;
            }
        } else if (y == null) {
            return LARGER;
        }

        if(x.Count() == 0) {
            if(y.Count() == 0) {
                return EQUAL;
            } else {
                return SMALLER;
            }
        } else if (y.Count() == 0) {
            return LARGER;
        }

        List<Card> a = x.getCards();
        List<Card> b = y.getCards();

        if(a.Count < b.Count) {
        	return SMALLER;
        } else if(a.Count > b.Count) {
        	return LARGER;
        } // Otherwise their length is equal

        if(CombinationChecker.allCardsSameColor(x)) {
        	if(!CombinationChecker.allCardsSameColor(y)) {
        		return LARGER;
        	}
        } else if(CombinationChecker.allCardsSameColor(y)) {
        	return SMALLER;
        } // More tests needed

       	// Het getal op de hoogste kaart telt
        if(a[a.Count - 1].getNumber() < b[b.Count - 1].getNumber()) {
        	return SMALLER;
        } else if(a[a.Count - 1].getNumber() > b[b.Count - 1].getNumber()) {
        	return LARGER;
        } // Otherwise their value is equal

        // Als getal hetzelfde is, kan kleur nog bepalen 
        Card.CardColor aColor = a[a.Count - 1].getColor();
        Card.CardColor bColor = b[b.Count - 1].getColor();

        return CompareColor(aColor, bColor);
	}

	/**
	 * -1 --> x < y
	 * 0  --> x = y
	 * 1  --> x > y
	 */
	public int CompareColor(Card.CardColor x, Card.CardColor y )  {
        if(x == Card.CardColor.Red) {
        	if(y == Card.CardColor.Red) {
        		return EQUAL;
        	} else {
        		return LARGER;
        	}
        } else if(y == Card.CardColor.Red) {
        	return SMALLER;
        }

        if(x == Card.CardColor.Green) {
        	if(y == Card.CardColor.Green) {
        		return EQUAL;
        	} else {
        		return LARGER;
        	}
        } else if(y == Card.CardColor.Green) {
        	return SMALLER;
        }

        return EQUAL;
	}
}