﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class CombinationChecker {
	public static int MIN_PRIME = 11;
	public static int MAX_PRIME = 89;

	/**
	 * Calculate the best value for the wildcard
	 * The combination will be updated if it is possible
	 */
	public static bool resolveWildcard(Combination combination) {
		List<Card> cards = combination.getCards();

		List<int> primes = generatePrimes();
		primes.Reverse();

		// Backup initial number
		Card wildcard = combination.getWildcard();
		int initialNumber = wildcard.getNumber();

		// In reverse order for best performance
		foreach(int possibleValue in primes) {
			wildcard.setNumber(possibleValue);
			combination.sort();

			if(isValidWildCardResolved(combination)) {
				// Set the best colour and return
				resolveColor(wildcard, combination);
				return true;
			}
		}

		// No solution found
		wildcard.setNumber(initialNumber);
		return false;
	}

	/**
	 * Calculate the best color for the wildcard
	 * The combination will always be updated
	 */
	private static void resolveColor(Card wildcard, Combination combination) {
		wildcard.setColor(Card.CardColor.Blue);

		if(allCardsSameColor(combination)) {
			return;
		} 

		wildcard.setColor(Card.CardColor.Green);

		if(allCardsSameColor(combination)) {
			return;
		}
	
		wildcard.setColor(Card.CardColor.Red);
	}

	/**
	 * Check if a combination is possible
	 * @pre if there is a wildcard, it's number must be set
	 */
	public static bool isValid(Combination combination) {
		if(combination == null) {
			return false;
		}

		List<Card> cards = combination.getCards();

		// Every combination has at least size 3
		if(cards.Count < 3) {
			return false;
		} else if(combination.getNumWildCards() > 0) {
			return resolveWildcard(combination);
		}  else {
			return isValidWildCardResolved(combination);
		}
	}

	private static bool isValidWildCardResolved(Combination combination) {
		List<Card> cards = combination.getCards();

		int difference = cards[1].getNumber() - cards[0].getNumber();

		if(difference == 0) {
			return false;
		}

		for(int i = 2; i < cards.Count; i++) {
			if(cards[i].getNumber() - cards[i - 1].getNumber() != difference * i) {
				//Debug.Log(combination.toRichText() + " -> NOT valid");
				return false;
			}
		}

		//Debug.Log(combination.toRichText() + " -> valid");
		return true;
	}

	/**
	 * Calculate the value/rank of a combination
	 */
	public static int calculateValue(Combination combination) {
		// Automaticcaly resolves wildcards
		if(!isValid(combination)) {
			return 0; // Rank 0
		}

		List<Card> cards = combination.getCards();
		int result = cards.Count - 2; // The rank = size - 2 in general

		// Combinations of size 9 have at least rank 8 (Scores Berekenen.xslx)
		if(cards.Count == 9) {
			result = 8;
		}

		// Combinations of size 8 and 9 gain an additial point if all cards have the same color
		if(cards.Count == 8 || cards.Count == 9) {
			if(allCardsSameColor(combination)) {
				result++;
			}
		}

		//Debug.Log(combination.toRichText() + " -> level " + result);

		return result;
	}

	/**
	 * Check if a number is a prime
	 */
	public static bool isPrime(int number) {
	    int boundary = (int) Mathf.Floor(Mathf.Sqrt(number));

	    if (number == 1) return false;
	    if (number == 2) return true;

	    for (int i = 2; i <= boundary; ++i)  {
	        if (number % i == 0)  return false;
	    }

	    return true;        
	}

	/**
	 * Return true if all cards have the same color
	 */
	public static bool allCardsSameColor(Combination combination) {
		List<Card> cards = combination.getCards();

		if(cards.Count < 1) {
			return true;
		}

		Card.CardColor firstColor = cards[0].getColor();

		for(int i = 1; i < cards.Count; i++) {
			if(firstColor != cards[i].getColor()) {
				return false;
			}
		}

		return true;
	}

	public static List<int> generatePrimes() {
		List<int> result = new List<int>();

		for(int i = MIN_PRIME; i <= MAX_PRIME; i++) {
			if(isPrime(i)) {
				result.Add(i);
			}
		}

		return result;
	}
}
