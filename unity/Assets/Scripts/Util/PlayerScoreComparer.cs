﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class PlayerScoreComparer : IComparer<Player> {
	public int Compare(Player x, Player y ) {
		int valueA = x.getScore();
		int valueB = y.getScore();
		return valueA.CompareTo(valueB);
	}
}
