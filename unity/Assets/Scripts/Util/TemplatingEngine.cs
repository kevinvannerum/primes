using System;
using System.Collections;
using System.IO;
using System.Xml;
 
using UnityEngine;
 
public class TemplatingEngine {
    public static string replace(string input, Hashtable replacements) {
    	foreach (string key in replacements.Keys) {
    		string newKey = "{" + key + "}";
    		input = input.Replace(newKey, replacements[key] as string);
    	}

    	return input;
    }
}