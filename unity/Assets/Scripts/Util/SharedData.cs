﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

namespace Model {
	public class SharedData : MonoBehaviour {
		private List<PlayerType> _players;
		
		public enum PlayerType {
			UI,
			AI_EASY,
			AI_MEDIUM,
			AI_HARD
		};

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		void Awake() {
	        DontDestroyOnLoad(transform.gameObject);
	    }

		public void setPlayers(List<PlayerType> players) {
			_players = players;
		}

		public List<PlayerType> getPlayers() {
			return _players;
		}
	}
}
