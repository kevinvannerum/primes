﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class IncrementContainerComparer : IComparer<IncrementContainer> {
	public int Compare(IncrementContainer x, IncrementContainer y ) {
		return new PlayerScoreComparer().Compare(x.getPlayer(), y.getPlayer());
	}
}
