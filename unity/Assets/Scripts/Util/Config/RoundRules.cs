using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
  
/**
 * Author: Sven Coppers
 * In-memory storage for the rules for one round
 */
public class RoundRules {
	private bool _combiningAllowed;
	private int _timeConstraint;

	public RoundRules() {
	}

	public bool isCombiningAllowed() {
		return _combiningAllowed;
	}

	public void setCombiningAllowed(bool combiningAllowed) {
		_combiningAllowed = combiningAllowed;
	}

	public void setTimeConstraint(int timeConstraint) {
		_timeConstraint = timeConstraint;
	}

	public int getTimeConstraint() {
		return _timeConstraint;
	}

	public string toString(Lang lang) {
		Hashtable replacements = new Hashtable();

		if (isCombiningAllowed()) {
			replacements.Add("can_must", lang.getString("rule_can"));
		} else {
			replacements.Add("can_must", lang.getString("rule_cannot"));
		}

		string result = "- " + lang.getString("rule_make_combination") + "\n";

		if(getTimeConstraint() != 0) {
			result += "- " + lang.getString("rule_time_constraint") + "\n";
			replacements.Add("time_constraint", "" + getTimeConstraint());
		}

		return TemplatingEngine.replace(result, replacements);
	}

	private string rangeToString(int min, int max, Lang lang) {
		if(max == 0) {
			return lang.getString("rule_none");
		} else if(min == max) {
			return "" + min;
		} else {
			return lang.getString("rule_between") + " " + min + " " + lang.getString("rule_and") + " " + max; 
		}
	}
}