using System;
using System.Collections;
using System.IO;
using System.Xml;
 
using UnityEngine;
 
public class Lang
{
    private Hashtable Strings;
 
    public Lang (string path, string language) {
        setLanguage(path, language);
    }
 
    /**
     * Use the setLanguage function to swap languages after the Lang class has been initialized.
     * This function is called automatically when the Lang class is initialized.
     * @param path = path to XML resource example:  Path.Combine(Application.dataPath, "lang.xml")
     * @param language = language to use example:  "English"
     */
    public void setLanguage (string path, string language) {
        Strings = new Hashtable();

        TextAsset stringsAsset = (TextAsset) Resources.Load("strings");
        XmlDocument xml = new XmlDocument();
        xml.LoadXml(stringsAsset.text); // load the file.
        XmlNodeList languageElements = xml.GetElementsByTagName(language);

        if(languageElements != null) {
            foreach (XmlNode languageElement in languageElements) {
                foreach (XmlNode languageItem in languageElement.ChildNodes) {
                    if(languageItem.NodeType != XmlNodeType.Comment) {
                        //Debug.Log("InnerText: " + languageItem.InnerText);
                        //Debug.Log("Name: " + languageItem.Attributes["name"].Value);
                        Strings.Add(languageItem.Attributes["name"].Value, languageItem.InnerText);
                    }
                }
            }
        } else {
            Debug.LogError("The specified language does not exist: " + language);
        }
    }
 
    /**
     * Access strings in the currently selected language by supplying this getString function with
     * the name identifier for the string used in the XML resource.
     */
    public string getString (string name) {
        if (!Strings.ContainsKey(name)) {
            Debug.LogError("The specified string does not exist: " + name);
         
            return "";
        }
 
        return (string)Strings[name];
    }
}