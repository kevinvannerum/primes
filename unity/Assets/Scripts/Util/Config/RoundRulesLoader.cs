using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
  
/**
 * Author: Sven Coppers
 * Loads the configuration into a list of RoundRules once. Each RoundRules contains the rules for a certain round.
 */
public class RoundRulesLoader {
	private List<RoundRules> _roundRules;

	public RoundRulesLoader() {
		_roundRules = new List<RoundRules>();

		loadRules();
	}

	public RoundRules getRoundRules(int roundNumber) {
		if(_roundRules.Count < roundNumber) {
			return null;
		} else {
			return _roundRules[roundNumber];
		}
	}

	public int getNumRounds() {
		return _roundRules.Count;
	}

	private void loadRules() {
		TextAsset roundRulesAsset = (TextAsset) Resources.Load("round_rules");
		XmlDocument xml = new XmlDocument();
		xml.LoadXml(roundRulesAsset.text); // load the file.
		XmlNodeList roundList = xml.GetElementsByTagName("round"); // array of the round nodes.

		foreach (XmlNode roundInfo in roundList) {
			RoundRules newRules = new RoundRules();

			foreach (XmlNode roundInfoItem in roundInfo.ChildNodes) {								
				if(roundInfoItem.Name == "allowCombining") {
					newRules.setCombiningAllowed(System.Convert.ToBoolean(roundInfoItem.InnerText));
				}  

				if(roundInfoItem.Name == "timeConstraint") {
					newRules.setTimeConstraint(System.Convert.ToInt32(roundInfoItem.InnerText));
				}
			}

			_roundRules.Add(newRules);
		}
	}
}