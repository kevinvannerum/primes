﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

public class RankLoader {
	public RankLoader() {

	}

	public static Dictionary<int, string> getRanks() {
		Dictionary<int, string> result = new Dictionary<int, string>();

		TextAsset ranksAsset = (TextAsset) Resources.Load("ranks");
		XmlDocument xml = new XmlDocument();
		xml.LoadXml(ranksAsset.text); // load the file.
		XmlNodeList rankList = xml.GetElementsByTagName("rank"); // array of the round nodes.

		foreach (XmlNode rankInfo in rankList) {
			int rank = 0;
			string title = "";

			foreach (XmlNode rankInfoItem in rankInfo.ChildNodes) {								
				if(rankInfoItem.Name == "value") {
					rank = System.Convert.ToInt32(rankInfoItem.InnerText);
				}  

				if(rankInfoItem.Name == "title") {
					title = rankInfoItem.InnerText;
				}
			}

			result.Add(rank, title);
		}

		return result;
	}
}