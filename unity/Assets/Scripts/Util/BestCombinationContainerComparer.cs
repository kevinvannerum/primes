﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class BestCombinationContainerComparer : IComparer<BestCombinationContainer>  {
	public int Compare(BestCombinationContainer x, BestCombinationContainer y ) {
		return new CombinationComparer().Compare(x.getCombination(), y.getCombination());
	}
}