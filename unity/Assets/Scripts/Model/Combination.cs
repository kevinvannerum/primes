﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Model{
	public class Combination {
		public List<Card> _cards;

		public Combination() {
			_cards = new List<Card>();
		}

		public Combination addCard(Card card) {
			_cards.Add(card);
			sort();
			return this;
		}

		public List<Card> getCards(){
			return _cards;
		}

		public void removeCard(Card c){
			_cards.Remove(c);
		}

		public bool cardInCombination(Card c){
			return _cards.Contains(c);
		}

		public Combination addCardsFromCombination(Combination combination){
			foreach( Card c in combination.getCards() ){
				addCard(c);
			}

			return this;
		}

		public void sort() {
			_cards = _cards.OrderBy(x => x.getNumber()).ToList();
		}

		public int Count(){
			return _cards.Count;
		}

		public int getNumWildCards() {
			int result = 0;

			foreach( Card c in _cards){
				if(c.isJoker()) {
					result++;
				}
			}

			return result;
		}

		public List<Card> removeAndGetAllCards(){
			List<Card> cards = _cards.ToList();
			_cards.Clear();
			return cards;
		}

		public Card getWildcard() {
			foreach(Card card in _cards) {
				if(card.isJoker()) {
					return card;
				}
			}

			return null;
		}

		public string toRichText() {
			if (_cards.Count == 0)
				return "";

			string result = _cards[0].toRichText();

			for(int i = 1; i < _cards.Count(); i++) {
				result = result + ", " + _cards[i].toRichText();
			}

			return result;
		}
	}
}
