﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class IncrementContainer {
	private int _increment;
	private Player _player;
	private string _explenation;

	public IncrementContainer(int increment, Player player, string explenation) {
		_increment = increment;
		_player = player;
		_explenation = explenation;
	}

	public Player getPlayer() {
		return _player;
	}

	public int getIncrement() {
		return _increment;
	}

	public string getExplenation() {
		return _explenation;
	}

	public string getRichIncrement() {
		if(_increment > 0) {
			return "<color=green>" + _increment + "</color>";
		} else {
			return "<color=red>" + _increment + "</color>";
		}
	}
}
