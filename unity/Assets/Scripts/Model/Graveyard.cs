using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

namespace Model{
	public class Graveyard {
		List<Card> _stack;

		public Graveyard(){
			_stack = new List<Card>();
		}

		public void clear() {
			_stack = new List<Card>();
		}

		public List<Card> getCardStack(){ 
			return _stack;
		}

		public void addCard(Card c){
			_stack.Add(c);
		}

		public Card popCard(){
			if(_stack.Count > 0) {
				Card local = _stack[_stack.Count - 1];
				_stack.RemoveAt(_stack.Count - 1);
				return local;
			} else {
				return null;
			}
		}

		public override string ToString(){
			string a = "";
			for(int i=0;i<_stack.Count;i++)
				a += _stack[i].getNumber() + _stack[i].getColor() + " ";
			return a;
		}

		public int Count() {
			return _stack.Count;
		}
	}
}
