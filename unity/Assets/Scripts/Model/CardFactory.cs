using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Model;

namespace Model {
	public class CardFactory : MonoBehaviour {
		public Transform _canvas; // 'Playfield' canvas
		public GameObject _deck;

		// Use this for initialization
		void Start () {
		}

		// Update is called once per frame
		void Update () {
		
		}

		public Card createCard(Model.Card.CardColor color, int number){
			// instantiate card
			GameObject o = Instantiate(
								Resources.Load("Card"), 
								new Vector3(0,0,0), 
								transform.rotation) as GameObject;

			// setup the card
			CardController cardController = o.GetComponent<CardController>();
			cardController.setupGameObject(color, number);

			resetPosition(cardController.getCard());

			return cardController.getCard();
		}

		public Card createCard(bool isJoker){
			// instantiate card
			GameObject o = Instantiate(
								Resources.Load("Card"), 
								new Vector3(0,0,0), 
								transform.rotation) as GameObject;

			// setup the card
			CardController cardController = o.GetComponent<CardController>();
			cardController.setupGameObject(Card.CardColor.Black, 0, true);

			resetPosition(cardController.getCard());

			return cardController.getCard();
		}

		public void resetPosition(Card card) {
			Transform t = card.getGraphic();

			// set the canvas as parent of this card		
			t.SetParent(_canvas, false); // o.transform.parent = _canvas;
			t.localScale = new Vector3(1.335679f, 1.335679f, 1.335679f);
			t.position = new Vector3(_deck.transform.position.x, _deck.transform.position.y, 100f);
			card.setCardVisibility(true);
		}
	}
}