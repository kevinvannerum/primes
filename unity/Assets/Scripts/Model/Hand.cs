﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Model {
	public class Hand {
		private List<Combination> _publicCards;
		private List<Card> _privateCards;

		public Hand(){
			_publicCards = new List<Combination>();
			_privateCards = new List<Card>();
		}

		public List<Card> getPrivateCards(){ return _privateCards; }
		public List<Combination> getPublicCards(){return _publicCards;}

		public void addCardToPrivate(Card card){
			_privateCards.Add(card);
		}

		public void removePrivateCard(Card card) {
			if (_privateCards.Contains(card))
				_privateCards.Remove(card);
		}

		public void removeCardFromPlayer(Card card){
			if (_privateCards.Contains(card))
				_privateCards.Remove(card);
			else {
				foreach(Combination comb in _publicCards){
					if (comb.cardInCombination(card))
						comb.removeCard(card);
				}
			}	
		}

		public Card removeLastCardInHand(){
			Card c = _privateCards.Last();
			_privateCards.Remove(c);
			return c;
		}

		public void addCombinationToPublic(Combination combination){
			_publicCards.Add(combination);
		}

		public void addCardToPublicCombination(int index, Card card){
			if (index < _publicCards.Count ){
				Combination c = _publicCards[index];
				c.addCard(card);
			}
		}

		public void clear() {
			_publicCards = new List<Combination>();
			_privateCards = new List<Card>();
		}

		public void moveCardInPrivateHand(Transform card, int newPosition){
			int oldPosition = 0;
			Card currentCard = null;

			foreach(Card c in _privateCards){
				if (c.getGraphic() == card){
					currentCard = c;
					break;
				}
				oldPosition++;
			}

			bool insertLast = false;
			if (newPosition == _privateCards.Count)
				insertLast = true;

			newPosition--;

			
			if (newPosition > oldPosition && insertLast==false)
				newPosition--;
			
			// Debug.Log(_privateCards.Count + " " + oldPosition + " " + newPosition  );

			_privateCards.RemoveAt(oldPosition);
			_privateCards.Insert(newPosition,currentCard);	
		}

		public void insertCardInPrivateHand(Card card, int newPosition){
			int oldPosition = 0;

			bool insertLast = false;
			if (newPosition == _privateCards.Count)
				insertLast = true;

			newPosition--;
			
			Debug.Log(_privateCards.Count + " " + oldPosition + " " + newPosition  );
			_privateCards.Insert(newPosition,card);	
		}

		public void insertCardInPrivateHand(Transform card, int newPosition){
			Card currentCard = null;

			// get card representative
			foreach(Card c in _privateCards){
				if (c.getGraphic() == card){
					currentCard = c;
					break;
				}
			}

			bool insertLast = false;
			if (newPosition == _privateCards.Count)
				insertLast = true;

			newPosition--;
			
			// Debug.Log(_privateCards.Count + " " + oldPosition + " " + newPosition  );
			_privateCards.Insert(newPosition,currentCard);	
		}
	}
}