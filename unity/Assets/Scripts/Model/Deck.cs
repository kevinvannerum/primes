﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Model{
	public class Deck {

		List<Card> _stack;

		public Deck(){
			_stack = new List<Card>();
		}

		public void clear() {
			_stack = new List<Card>();
		}

		public List<Card> getCardStack(){ 
			return _stack;
		}

		public void addCard(Card c){
			_stack.Add(c);
		}

		public Card popCard(){
			if(_stack.Count > 0) {
				Card local = _stack[_stack.Count - 1];
				_stack.RemoveAt(_stack.Count - 1);
				return local;
			} else {
				return null;
			}
		}

		public void shuffle() {  
		    int n = _stack.Count;  
		    while (n > 1) {  
		        n--;  
		        int k = Random.Range(0,n + 1);  
		        Card value = _stack[k];  
		        _stack[k] = _stack[n];  
		        _stack[n] = value;  
		    }  
		}

		public override string ToString(){
			string a = "";
			for(int i=0;i<_stack.Count;i++)
				a += _stack[i].getNumber() + _stack[i].getColor().ToString() + " ";
			return a;
		}
	}
}
