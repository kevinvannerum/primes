﻿using UnityEngine;
using System.Collections;

namespace Model{

	[System.Serializable]
	public class Card {

		public enum CardColor {
			Red,
			Green,
			Blue,
			Black
		}; 

		Transform _graphic;

		private bool _cardShown = false;
		private bool _isJoker = false;

		public CardColor _cardColor = CardColor.Red;
		public int _primeNumber = 11;
		private float _animationStart;

		public Card(bool jocker) {
			_primeNumber = 1;
			_cardColor = CardColor.Red;
			_isJoker = jocker;
			_animationStart = Time.time;
		}

		public Card(CardColor color, int number, bool isJoker = false) {
			_primeNumber = number;
			_cardColor = color;
			_isJoker = isJoker;
		}

		public Card(CardColor color, int number, Transform t, bool isJoker = false) {
			_primeNumber = number;
			_cardColor = color;
			_graphic = t;
			_isJoker = isJoker;
		}

		public void setColor(CardColor c) {_cardColor = c;}
		public void setNumber(int n) {_primeNumber=n;}

		public CardColor getColor() {return _cardColor;}
		public int getNumber() {return _primeNumber;}
		public bool isJoker() {return _isJoker;}

		public void setGraphic(Transform t){
			_graphic = t;
		}

		public Transform getGraphic(){
			return _graphic;
		}

		override public string ToString(){
			return _primeNumber + _cardColor.ToString() + "";
		}

		public void setAnimationStart() {
			_animationStart = Time.time;
		}

		public float getAnimationStart() {
			return _animationStart;
		}

		public bool isVisible(){ return _cardShown;}

		public void setCardVisibility(bool visible) {
			if (_cardShown == visible)
				return;

			Transform num = _graphic.transform.Find("number");
			Transform frontTx = _graphic.transform.Find("texture");
			Transform backTx = _graphic.transform.Find("backTexture");

			_cardShown = visible;
			if (visible){
				num.gameObject.SetActive(true);
				frontTx.gameObject.SetActive(true);
				backTx.gameObject.SetActive(false);
			} else {
				num.gameObject.SetActive(false);
				frontTx.gameObject.SetActive(false);
				backTx.gameObject.SetActive(true);
			}
		}

		public string toRichText() {
			if(isJoker()) {
				return "<color=white>J</color>";
			} else {
				switch (_cardColor) {
					case CardColor.Red:
						return "<color=red>" + _primeNumber + "</color>";
					case CardColor.Green:
						return "<color=green>" + _primeNumber + "</color>";
					case CardColor.Blue:
						return "<color=blue>" + _primeNumber + "</color>";
					default:
					  	return "<color=white>" + _primeNumber + "</color>";
				}
			}
		}
	}
}