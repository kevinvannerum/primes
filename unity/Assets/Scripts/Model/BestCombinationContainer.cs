﻿using UnityEngine;
using System.Collections;
using Model;

public class BestCombinationContainer {
	private Combination _combination;
	private Player _player;

    public BestCombinationContainer(Combination combination, Player player) {
    	_combination = combination;
    	_player = player;
    }

    public Combination getCombination() {
    	return _combination;
    }

    public Player getPlayer() {
    	return _player;
    }
}
