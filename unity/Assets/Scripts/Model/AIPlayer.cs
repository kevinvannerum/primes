using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

namespace Model {
	public class AIPlayer: Player {
		private int _roundNumberToMakeCombination;
		private int _maximumCombinationSize;

		public AIPlayer(int id, GameController gameController, int maximumCombinationSize) : base(id, gameController) {
			// In which round will it start trying to make a combination?
			_roundNumberToMakeCombination = Random.Range(2, 4); // Round 2, 3 or 4

			// What is the maximum size of our combination?
			//_maximumCombinationSize = Random.Range(3, 7); // Not to strong
			_maximumCombinationSize = maximumCombinationSize;
			
			_isAI = true;
		}

		public override string getName() {
			return "Computer " + getId();
		}

		public override IEnumerator doTurn(int roundNumber, RoundRules roundRules) {
       		yield return new WaitForSeconds(1);

       		// Grab a card from the deck
       		Card lastCard = _deck.popCard();
			_hand.addCardToPrivate(lastCard);

			// Make a combination this round
       		if(roundNumber >= _roundNumberToMakeCombination - 1 && roundRules.isCombiningAllowed()) {
       			Combination combination = searchValidCombination();

       			if(combination != null) {
       				confirmCombination(combination);
       			}
       		}

       		removeRandomCard();

			endTurn();
		}

		private Combination searchValidCombination() {
			List<Card> cards = _hand.getPrivateCards();

			// Try every possible size, but start from the biggest one
   			for(int i = _maximumCombinationSize; i >= 3; i--) {
   				List<List<int>> combinationIndecesList = new List<List<int>>();
				List<int> prefix = new List<int>();
				makeCombinations(i, 0, cards.Count - 1, prefix, combinationIndecesList);

				// For every generated list of usable indexes
				foreach (List<int> combinationIndeces in combinationIndecesList) {
					Combination combination = new Combination();
					int teller = 0;

					// Add all cards with these indexes to the combination
					foreach (int index in combinationIndeces) {
						combination.addCard(cards[index]);
						teller++;
					}

					// Check if the combination is valid 
					if(CombinationChecker.isValid(combination)) {
						//Debug.Log(combination.toRichText() + " -> valid");
						return combination;
					} else {
						//Debug.Log(combination.toRichText() + " -> NOT valid");
					}
				}

				// Debug.Log("Num combinations of size " + _maximumCombinationSize + " out of " + cards.Count + " : " + combinationIndecesList.Count);
   			}

   			return null;
		}

		private void confirmCombination(Combination combination) {
			List<Card> privateCards = getHand().getPrivateCards();

			foreach(Card card in combination.getCards()) {
				privateCards.Remove(card);
			}

			_hand.addCombinationToPublic(combination);

			Debug.Log(getName() + " made the combination: " + combination.toRichText()); 
		}

		/**
		 * Every level a card will be chosen. All cards will be in the range of [leftBound, RightBound] 
		 */
		private void makeCombinations(int cardsLeft, int leftBound, int rightBound, List<int> prefix, List<List<int>> combinations) {
			if(cardsLeft > 0) {
				for(int i = leftBound; i <= rightBound - cardsLeft + 1; i++) {
					prefix.Add(i);
					makeCombinations(cardsLeft - 1, i + 1, rightBound, prefix, combinations);
					prefix.Remove(i);
				}
			} else {
				combinations.Add(new List<int>(prefix));
			}
		}

		/*private void printCombination(List<int> combination) {
			string result = "" + combination[0];

			for(int i = 1; i < combination.Count; i++) {
				result = result + ", " + combination[i];
			}

			Debug.Log(result);
		}*/
	}
}