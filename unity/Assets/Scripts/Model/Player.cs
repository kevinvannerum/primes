﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

namespace Model {
	public abstract class Player {
		protected Hand _hand;
		protected int _score;
		protected int _id;
		protected RoundRulesLoader _roundRulesLoader;
		protected GameController _gameController;
		protected Graveyard _graveyard;
		protected Deck _deck;
		
		protected bool _isAI = false;

		public Player(int id, GameController gameController) {
			_id = id;
			_gameController = gameController;
			_roundRulesLoader = _gameController.getRoundRulesLoader();
			_score = _gameController.getStartScore();
			_graveyard = _gameController.getGraveyard();
			_deck = _gameController.getDeck();
			_hand = new Hand();
		}

		public int getScore() {
			return _score;
		}

		// Also works when the increment is negative
		public void incrementScore(int increment) {
			_score = _score + increment;
		}

		public Hand getHand() { 
			return _hand; 
		}

		public int getId() {
			return _id;
		}

		public void newGame() {
			_hand.clear();
		}

		public abstract string getName();

		public abstract IEnumerator doTurn(int roundNumber, RoundRules roundRules);
		
		public void endTurn() {
			_gameController.endTurn();
		}

		protected void removeRandomCard() {
			List<Card> cards = _hand.getPrivateCards();
			int cardToRemoveIndex = (int) (Random.value * (cards.Count - 1));
			Card cardToRemove = cards[cardToRemoveIndex];
			
			_hand.removePrivateCard(cardToRemove);
			_graveyard.addCard(cardToRemove);
		}

		public bool isAI() { return _isAI; }

	}
}