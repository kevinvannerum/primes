using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

namespace Model {
	public class UIPlayer: Player  {
		private PlayerTurnPanelController _playerTurnPanelController;
		private Card _lastCard;
		private bool _hasPickedCard;
		private bool _hasPickedFromGraveyard;
		
		public UIPlayer(int id, GameController gameController) : base(id, gameController) {
			_playerTurnPanelController = gameController.getPlayerTurnPanelController();
		}

		public override string getName() {
			return "Player " + getId();
		}

		public override IEnumerator doTurn(int roundNumber, RoundRules roundRules) {
			// Start listening to UI updates
			_playerTurnPanelController.setCurrentPlayer(this);

			// If there is a time limit, start it
			if(roundRules.getTimeConstraint() > 0) {
				_playerTurnPanelController.startTimer(roundRules.getTimeConstraint());
			}

			_playerTurnPanelController.startTurn(_graveyard.Count(), roundRules.isCombiningAllowed());
			_hasPickedCard = false;

			yield return new WaitForSeconds(0);
		}

		/**
		 * Afnemen van kerkhof
		 */
		public void pickLastCard() {
			_lastCard = _graveyard.popCard();
			_hand.addCardToPrivate(_lastCard);

			/* Add card to pickable this turn */
			FirstPersonCardPainter g = GameObject.Find("FirstPersonCardPainter").GetComponent<FirstPersonCardPainter>();
			g.addCardToPickableThisTurn(_lastCard);
			g.graveyardorDeckCardGrabbed(_lastCard);

			_hasPickedCard = true;
			_hasPickedFromGraveyard = true;
			_lastCard.setAnimationStart();
		}

		/**
		 * Grab a new card from the deck
		 */
		public void pickNewCard() {
			_lastCard = _deck.popCard();
			_hand.addCardToPrivate(_lastCard);

			/* Add card to pickable this turn */
			FirstPersonCardPainter g = GameObject.Find("FirstPersonCardPainter").GetComponent<FirstPersonCardPainter>();
			g.addCardToPickableThisTurn(_lastCard);
			g.graveyardorDeckCardGrabbed(_lastCard);

			_hasPickedCard = true;
			_hasPickedFromGraveyard = false;
			_lastCard.setAnimationStart();
		}

		/**
		 * Remove last new card from player hand
		 */
		public void undoLastCard() {
			_hand.removePrivateCard(_lastCard); // Untested
			_graveyard.addCard(_lastCard);
			_lastCard.setAnimationStart();
		}

			
		public void finishTurn(Card discardedCard) {
			// _hand.removePrivateCard(discardedCard); // Untested
			_hand.removeCardFromPlayer(discardedCard); // removes from both private and public cards :)
			_graveyard.addCard(discardedCard);
			endTurn();
		}

		/**
		 * What should happen when the player's time is Up
		 */
		public void timeOut() {
			/* Drop a currently dragged card */
			FirstPersonCardPainter g = GameObject.Find("FirstPersonCardPainter").GetComponent<FirstPersonCardPainter>();
			g.dropDraggedCard();

			if(!_hasPickedCard) { // has not picked card
				pickNewCard();
				removeRandomCard();
			} else { // has picked card
				Card c = _hand.removeLastCardInHand(); // Untested
				_graveyard.addCard(c);
			}

			endTurn();
		}

		public bool hasPickedCard() {
			return _hasPickedCard;
		}

		public bool hasPickedFromGraveyard() {
			return _hasPickedFromGraveyard;
		}

		public Card getLastCard() {
			return _lastCard;
		}
	}
}