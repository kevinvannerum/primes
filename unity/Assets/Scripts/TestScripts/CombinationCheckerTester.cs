using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;

public class CombinationCheckerTester : ScriptableObject {
	private Combination _valid, _invalid, _tribal, _primePlus, _prime, _mummie, _mum;
	private int _numErrors;

	public CombinationCheckerTester() {
		init();
	}

	public void runTests() {
		_numErrors = 0;

		testIsValid();
		testCalculateValue();
		testWildcardIsValid();
		testWildCardCalculateValue();

		if(_numErrors == 0) {
			Debug.Log("All CombinationChecker Unit Tests were performed correctly");
		}
	}

	private void init() {
		_valid = new Combination();
		_valid.addCard(new Card(Card.CardColor.Red, 11));
		_valid.addCard(new Card(Card.CardColor.Green, 13));
		_valid.addCard(new Card(Card.CardColor.Blue, 17));

		_invalid = new Combination();
		_invalid.addCard(new Card(Card.CardColor.Red, 13));
		_invalid.addCard(new Card(Card.CardColor.Green, 17));

		_tribal = new Combination();
		_tribal.addCard(new Card(Card.CardColor.Red, 11));
		_tribal.addCard(new Card(Card.CardColor.Green, 13));
		_tribal.addCard(new Card(Card.CardColor.Blue, 17));

		_mummie = new Combination();
		_mummie.addCard(new Card(Card.CardColor.Red, 17));
		_mummie.addCard(new Card(Card.CardColor.Red, 19));
		_mummie.addCard(new Card(Card.CardColor.Red, 23));
		_mummie.addCard(new Card(Card.CardColor.Red, 29));
		_mummie.addCard(new Card(Card.CardColor.Red, 37));
		_mummie.addCard(new Card(Card.CardColor.Red, 47));
		_mummie.addCard(new Card(Card.CardColor.Blue, 59));
		_mummie.addCard(new Card(Card.CardColor.Red, 73));

		_mum = new Combination();
		_mum.addCard(new Card(Card.CardColor.Red, 17));
		_mum.addCard(new Card(Card.CardColor.Red, 19));
		_mum.addCard(new Card(Card.CardColor.Red, 23));
		_mum.addCard(new Card(Card.CardColor.Red, 29));
		_mum.addCard(new Card(Card.CardColor.Red, 37));
		_mum.addCard(new Card(Card.CardColor.Red, 47));
		_mum.addCard(new Card(Card.CardColor.Red, 59));
		_mum.addCard(new Card(Card.CardColor.Red, 73));

		_prime= new Combination();
		_prime.addCard(new Card(Card.CardColor.Red, 17));
		_prime.addCard(new Card(Card.CardColor.Red, 19));
		_prime.addCard(new Card(Card.CardColor.Red, 23));
		_prime.addCard(new Card(Card.CardColor.Red, 29));
		_prime.addCard(new Card(Card.CardColor.Red, 37));
		_prime.addCard(new Card(Card.CardColor.Red, 47));
		_prime.addCard(new Card(Card.CardColor.Red, 59));
		_prime.addCard(new Card(Card.CardColor.Red, 73));
		_prime.addCard(new Card(Card.CardColor.Blue, 89));

		_primePlus = new Combination();
		_primePlus.addCard(new Card(Card.CardColor.Red, 17));
		_primePlus.addCard(new Card(Card.CardColor.Red, 19));
		_primePlus.addCard(new Card(Card.CardColor.Red, 23));
		_primePlus.addCard(new Card(Card.CardColor.Red, 29));
		_primePlus.addCard(new Card(Card.CardColor.Red, 37));
		_primePlus.addCard(new Card(Card.CardColor.Red, 47));
		_primePlus.addCard(new Card(Card.CardColor.Red, 59));
		_primePlus.addCard(new Card(Card.CardColor.Red, 73));
		_primePlus.addCard(new Card(Card.CardColor.Red, 89));
	}

	private void testIsValid() {
		if(!CombinationChecker.isValid(_valid)) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - isValid(): Valid Combination not recognized");
		}

		if(CombinationChecker.isValid(_invalid)) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - isValid(): Invalid Combination not detected");
		}
	}

	private void testCalculateValue() {
		if(CombinationChecker.calculateValue(_tribal) != 1) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - calculateValue: Tribal not recognized");
		}

		if(CombinationChecker.calculateValue(_mummie) != 6) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - calculateValue: Mummie not recognized");
		}

		if(CombinationChecker.calculateValue(_mum) != 7) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - calculateValue: Mum not recognized");
		}
		
		if(CombinationChecker.calculateValue(_prime) != 8) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - calculateValue: Prime not recognized");
		}
		
		if(CombinationChecker.calculateValue(_primePlus) != 9) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - calculateValue: Prime+ not recognized");
		}
	}

	private void testWildcardIsValid() {
		Combination combination = new Combination();
		combination.addCard(new Card(Card.CardColor.Blue, 17));
		combination.addCard(new Card(Card.CardColor.Red, 19));
		combination.addCard(new Card(Card.CardColor.Blue, 23));
		combination.addCard(new Card(Card.CardColor.Green, 29));
		combination.addCard(new Card(Card.CardColor.Green, 37));
		combination.addCard(new Card(true));
		combination.addCard(new Card(Card.CardColor.Red, 59));
		combination.addCard(new Card(Card.CardColor.Red, 73));

		if(!CombinationChecker.isValid(combination)) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - testWildcardIsValid: Mummie with wildcard not recognized");
		}

		Combination combination2 = new Combination();
		combination2.addCard(new Card(Card.CardColor.Blue, 17));
		combination2.addCard(new Card(Card.CardColor.Blue, 19));
		combination2.addCard(new Card(Card.CardColor.Blue, 23));
		combination2.addCard(new Card(Card.CardColor.Blue, 29));
		combination2.addCard(new Card(Card.CardColor.Blue, 37));
		combination2.addCard(new Card(true));
		combination2.addCard(new Card(Card.CardColor.Blue, 59));
		combination2.addCard(new Card(Card.CardColor.Blue, 73));

		if(!CombinationChecker.isValid(combination2)) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - testWildcardIsValid: Mum with wildcard not recognized");
		}
	}

	private void testWildCardCalculateValue() {
		Combination combination = new Combination();
		combination.addCard(new Card(Card.CardColor.Blue, 17));
		combination.addCard(new Card(Card.CardColor.Red, 19));
		combination.addCard(new Card(Card.CardColor.Blue, 23));
		combination.addCard(new Card(Card.CardColor.Green, 29));
		combination.addCard(new Card(Card.CardColor.Green, 37));
		combination.addCard(new Card(true));
		combination.addCard(new Card(Card.CardColor.Red, 59));
		combination.addCard(new Card(Card.CardColor.Red, 73));

		if(CombinationChecker.calculateValue(combination) != 6) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - testWildCardCalculateValue: Value for mummie with wildcard is wrong");
		}

		Combination combination2 = new Combination();
		combination2.addCard(new Card(Card.CardColor.Blue, 17));
		combination2.addCard(new Card(Card.CardColor.Blue, 19));
		combination2.addCard(new Card(Card.CardColor.Blue, 23));
		combination2.addCard(new Card(Card.CardColor.Blue, 29));
		combination2.addCard(new Card(Card.CardColor.Blue, 37));
		combination2.addCard(new Card(true));
		combination2.addCard(new Card(Card.CardColor.Blue, 59));
		combination2.addCard(new Card(Card.CardColor.Blue, 73));

		if(CombinationChecker.calculateValue(combination2) != 7) {
			_numErrors++;
			Debug.Log("CombinationCheckerTester - testWildCardCalculateValue: Value for mum with wildcard is wrong");
		}
	}
}
