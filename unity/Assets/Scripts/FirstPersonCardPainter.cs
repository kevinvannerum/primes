using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Model;
using System.Linq;

public class FirstPersonCardPainter : MonoBehaviour {

	private UIPlayer _player = null;
	public const float MAX_CARDS = 10f;

	Transform _draggedCard;
	Vector3 _dragOffset;

	public GameObject[] publicSlotStart;
	public GameObject privateSlotStart;
	public float publicCardsDrawRegionWidth = 326;
	public float privateCardsDrawRegionWidth = 326;

	public GameObject dropRegionGameobject;
	public GameObject combinationPickerButtons;
	public GameObject combinationHighlight;
	public GameObject graveyardHighlight;
	public GameObject graveyard;
	public PlayerTurnPanelController _playerTurnPanelController;

	private List<Card> _cardsHeldInHandThisRound;
	private int _numberOfCombinationInHand = 0;
	private RoundRules _currentRoundRules;

	private bool _grabbedCardFromDeckOrGraveyard = false;
	private Card _pGrabbedCardFromDeckOrGraveyard = null;

	public FirstPersonCardPainter() {
		_cardsHeldInHandThisRound = new List<Card>();
	}

	public void setPlayerTurnPanelController(PlayerTurnPanelController playerTurnPanelController) {
		_playerTurnPanelController = playerTurnPanelController;
	}

	void Update(){
		if (_player == null)
			return;

		dragAndDropRoutine();
		drawCards();
	}

	/**
	 * _currentRoundRules is automatically updated every round
	 */
	public void updateRoundRules(RoundRules roundRules) {
		_currentRoundRules = roundRules;
	}

	private void dragAndDropRoutine(){
		// Gets the mouse position in the form of a ray.
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Input.GetMouseButtonDown(0)) { // mouse click
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

            // See if an object is beneath us using raycasting.
            if (hit != null && hit.transform != null && hit.transform.tag == "PlayerCard") {

        		// If we hit an object then hold on to the object.
            	_draggedCard = hit.transform;  

            	// This is so when you click on an object its center does not align with mouse position.
          		_dragOffset = _draggedCard.position-ray.origin;

          		// Cards that were in the HAND of the player in THIS TURN can be moved, others can't
            	if (!_cardsHeldInHandThisRound.Contains(getCardFromTransform(_draggedCard)))
            		_draggedCard = null;
            	 
        	} else if (hit != null && hit.transform != null && hit.transform.tag == "CombinationPlaceholder"){
        		// enableCombinationMakerInitiator(hit);
        	}
        }

        // drop card if mouse button release
        else if (Input.GetMouseButtonUp(0) && _draggedCard != null) {

        	// Drop into a combination if hovering above a public combination region
			int draggedCardOverPublicRegion = draggedCardInPublicHandRegion();
			if (draggedCardOverPublicRegion >= 0 &&  draggedCardOverPublicRegion <= _numberOfCombinationInHand ){ // only if region >=0 and no region 'too far ahead'
				// this is the combination we try to add a new card to
				Combination relevantCombination = _player.getHand().getPublicCards()[draggedCardOverPublicRegion];

				// check validity of possible new combination
        		bool isValid = isValidCombinationUponCardDrop(draggedCardOverPublicRegion, _draggedCard);

        		// test if dropping combinations is allowed during this round
        		if(_currentRoundRules.isCombiningAllowed() && _grabbedCardFromDeckOrGraveyard) {

        			// allow adding if count < 3
		        	// allow adding if new combination >= 3 && valid
        			if(_player.getHand().getPrivateCards().Count > 1) {
						// Debug.Log("is valid: " + isValid);
						// Debug.Log("revelantcomb count: " + relevantCombination.Count());

	        			if ( ( (relevantCombination.Count()+1) >= 3 && isValid) || relevantCombination.Count() < 2){
		        			// make a new combination containing the dragged card
							Card c = getCardFromTransform(_draggedCard);
							removeFromPrivatePublicHand(c);
							relevantCombination.addCard(c);
		        		}
        			} else {
        				// You must drop the last card instead of combining it
        				_playerTurnPanelController.showInfoLastCard();
        			}
        		} else {
        			// Combining is not allowed in this round
        		}
			}
			
			/* if the card got dropped in the 'card drop' region */
			else if ( draggedCardInDropRegion() ) {
				//Debug.Log("wow this really works");

				Combination relevantCombination = _player.getHand().getPublicCards()[_numberOfCombinationInHand];

				// check validity of possible new combination
				bool isValid = isValidCombinationUponCardDrop(_numberOfCombinationInHand, _draggedCard);

				// test if dropping combinations is allowed during this round
        		if(_currentRoundRules.isCombiningAllowed() && _grabbedCardFromDeckOrGraveyard) {

        			// allow adding if count < 3
		        	// allow adding if new combination >= 3 && valid
        			if(_player.getHand().getPrivateCards().Count > 1) {
						// Debug.Log("is valid: " + isValid);
						// Debug.Log("revelantcomb count: " + relevantCombination.Count());

	        			if ( ( (relevantCombination.Count()+1) >= 3 && isValid) || relevantCombination.Count() < 2){
		        			// make a new combination containing the dragged card
							Card c = getCardFromTransform(_draggedCard);
							removeFromPrivatePublicHand(c);
							relevantCombination.addCard(c);
		        		}
        			} else {
        				// You must drop the last card instead of combining it
        				_playerTurnPanelController.showInfoLastCard();
        			}
        		} else {
        			// Combining is not allowed in this round
        		}

 			}

			else if (draggedCardInPrivateHandRegion()) {
		        // check grid position -> re-insert at the proper spot (true is for private insertion)
		        insertCardAtNewPosition(_draggedCard, potentialPositionInPrivateHand(_draggedCard), true);      
        	} 
        	else if(draggedCardInGraveyardRegion()) {
        		// If the user picked a card from the graveyard
				if(_player.hasPickedFromGraveyard()) {
					// that card must be used in a valid combination or being currently dragged & dropped
					if(_draggedCard == _player.getLastCard().getGraphic() || graveyardCardInValidCombination()) {
        				//Debug.Log("Dropped in graveyard");
						_playerTurnPanelController.finishTurnAction(getCardFromTransform(_draggedCard));
					}
				} else {
					//Debug.Log("Dropped in graveyard");
					_playerTurnPanelController.finishTurnAction(getCardFromTransform(_draggedCard));
				}
        	} 

        	// Else just drop the card
        	dropDraggedCard();
        }

        // if we're dragging a card -> drag card
        if (_draggedCard != null) {
            _draggedCard.transform.position = new Vector3(
            				ray.origin.x+_dragOffset.x, 
            				ray.origin.y+_dragOffset.y,
            				90);     // Only move the object on a 2D plane.

            adjustCombinationHighlights();
        }
	}

	// returns true if player has a valid combination with a grabbed card from the graveyard
	private bool graveyardCardInValidCombination(){
		foreach(Combination comb in _player.getHand().getPublicCards()){
			// if the grabbed card from graveyard is in one of the combinations ...
			if (comb.cardInCombination(_pGrabbedCardFromDeckOrGraveyard) == true){
				// check validity ...
				return CombinationChecker.isValid(comb);
			}
		}
		return false;
	}

	private void adjustCombinationHighlights(){
		// if hovering above some region --> highlight !
        int region = draggedCardInPublicHandRegion();

        if (region < 0 || region >=4 ){
        	combinationHighlight.SetActive(false);
        }
        else if (region <= _numberOfCombinationInHand){
			// No feed forward when combining is not allowed
			if(!_currentRoundRules.isCombiningAllowed()) {
				return;
			}

			// Prevent the player from combining his last card, because that one should be dropped
			if(_player.getHand().getPrivateCards().Count < 2) {
				return;
			}

			// Don't hightlight when no cards are present
			if (getNumberOfCardsInCombination(region) == 0)
				return;

			combinationHighlight.SetActive(true);

        	combinationHighlight.transform.position = 
        				new Vector3(
							publicSlotStart[region].transform.position.x,
							publicSlotStart[region].transform.position.y,
							110);

        	// if a card is ACTUALLY hovering above a region --> color highlight
        	if (_draggedCard != null){	
        		// check validity
        		bool isValid = isValidCombinationUponCardDrop(region, _draggedCard);

        		// if the combination is invalid, the highlight changes color
        		Transform tex = combinationHighlight.transform.Find("texture");
        		if (isValid){
        			tex.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
        		}
        		else{
        			tex.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
        		}
        	}
        }

        graveyardHighlight.SetActive(draggedCardInGraveyardRegion());
	}

	private bool isValidCombinationUponCardDrop(int combinationIndex, Transform card){
		Combination comb = new Combination();
		comb.addCardsFromCombination(_player.getHand().getPublicCards()[combinationIndex]);
		Card c = getCardFromTransform(card);
		comb.addCard(c);

		// check validity
		return CombinationChecker.isValid(comb);
	}

	private int getNumberOfCardsInCombination(int combinationIndex){
		return _player.getHand().getPublicCards()[combinationIndex].Count();
	}

	public void graveyardorDeckCardGrabbed(Card c){
		_grabbedCardFromDeckOrGraveyard = true;
		_pGrabbedCardFromDeckOrGraveyard = c;
	}

	public void dropDraggedCard(){
		_draggedCard = null;
        adjustCombinationHighlights();
	}

	// this is called when a new '+' combination sign is clicked
	private void enableCombinationMakerInitiator(RaycastHit2D hit){
		int region = transformInPublicHandRegion(hit.transform);
		
		combinationPickerButtons.SetActive(true);

		// reposition combination buttons confirmations
		if (region >= 0 && region < 4){
			combinationPickerButtons.transform.position = new Vector3(
														publicSlotStart[region].transform.position.x,
														publicSlotStart[region].transform.position.y,
														500);
		}

	}

	private void insertCardAtNewPosition(Transform droppedCard, int potentialPosition, bool privateHand){
		// card sits in private hand
		if (cardIsInPrivateHand(droppedCard))
			_player.getHand().moveCardInPrivateHand(droppedCard, potentialPosition);

		// card sits on the table
		else {
			// remove from the combination on the table
			Card c = getCardFromTransform(_draggedCard);
			removeFromPrivatePublicHand(c);

			// and then just insert it
			_player.getHand().insertCardInPrivateHand(c, potentialPosition);
		}
	}

	// this is set on a a new turn --> special things happen here !!!
	public FirstPersonCardPainter setPlayer(UIPlayer p){
		_player = p;
		_cardsHeldInHandThisRound = _player.getHand().getPrivateCards().ToList();
		_numberOfCombinationInHand = numberOfMadeCombinations(_player);
		_grabbedCardFromDeckOrGraveyard = false;
		_pGrabbedCardFromDeckOrGraveyard = null;
//		Debug.Log("numberofcombs: " + _numberOfCombinationInHand);
		return this;
	}

	public int numberOfMadeCombinations(Player p){
		List<Combination> list = p.getHand().getPublicCards();
		int count = 0;

		foreach (Combination c in list){
			if (c.Count() >= 3 )
				++count;
		}

		return count;
	}

	public void endTurnProcedures(){
		List<Combination> publicCards = _player.getHand().getPublicCards();
		// List<Combination> combinationToReject = new List<Combination>();

		// retrieve which combinations are invalid and remove them
		foreach( Combination comb in publicCards){
			bool isValid = CombinationChecker.isValid(comb);

			// not valid --> remove cards and add back to hand
			if (!isValid){
				// combinationToReject.Add(comb);
				List<Card> cards = comb.removeAndGetAllCards();
				foreach(Card c in cards){
					_player.getHand().addCardToPrivate(c);
				}
			}
		}

	}

	public FirstPersonCardPainter drawCards(){
		Hand hand = _player.getHand();

		// To always have the most up to date cards
		//_cardsHeldInHandThisRound = hand.getPrivateCards().ToList();

		drawPrivateCards(hand);
		drawPublicCards(hand);
		return this;
	}

	public void addCardToPickableThisTurn(Card c){
		_cardsHeldInHandThisRound.Add(c);
	}

	// draw private hand
	private void drawPrivateCards(Hand hand){
		List<Card> privateCards = hand.getPrivateCards();

		GameObject start = privateSlotStart;
		float stride = privateCardsDrawRegionWidth;
		float offset = 0;
		float count = 0;

		foreach(Card c in privateCards){
			// make card visible
			c.setCardVisibility(true);

			if (c.getGraphic() == _draggedCard){
				offset += stride / MAX_CARDS; // 10 cards
				count++;
				continue; // don't arrange a dragged card
			}

			// make a gap for the potential position of the card
			if (_draggedCard != null){ 
				if (count+1 == potentialPositionInPrivateHand(_draggedCard) &&
								potentialPositionInPrivateHand(_draggedCard)-1 < privateCards.Count &&
								draggedCardInPrivateHandRegion() ){
					offset += stride / MAX_CARDS; // 10 cards
					count++;
				}
			}

			Transform f = c.getGraphic();
			Vector3 from = f.position;
			Vector3 to = new Vector3(
									start.transform.position.x + offset,
									start.transform.position.y,
									100 + count	);

        	float step = 500.0f * Time.deltaTime;
			f.position = Vector3.MoveTowards(from, to, step);

			f.localScale = new Vector3(1.335679f, 1.335679f, 1.335679f);

			offset += stride / MAX_CARDS; // 10 cards
			count++;
		}
	}

	// draw oublic hand
	private void drawPublicCards(Hand hand){
		List<Combination> publicCards = hand.getPublicCards();

		int combinationCount = 0;
		float offset = 0;
		float count = 0;
		GameObject start;
		float stride = publicCardsDrawRegionWidth;
		// place the cards where needed
		foreach(Combination comb in publicCards){

			// array overflow protection
			if (combinationCount > publicCards.Count){
				Debug.Log("Too much shitty combinations");
				return;
			}

			start = publicSlotStart[combinationCount];
			foreach (Card c in comb.getCards()){
				// don't arrange a dragged card
				if (c.getGraphic() == _draggedCard){
					offset += stride / MAX_CARDS; // 10 cards
					count++;
					continue; 
				}

				// show card
				c.setCardVisibility(true);

				Transform f = c.getGraphic();
				Vector3 from = f.position;
				Vector3 to = new Vector3(
										start.transform.position.x + offset,
										start.transform.position.y,
										100 + count);

			/*	float journeyLength = Vector3.Distance(from, to);
				float distCovered = (Time.time - c.getAnimationStart()) * 1.0;
        		float fracJourney = distCovered / journeyLength;

				f.position = Vector3.Lerp(from, to, fracJourney);*/
				// f.position = to;

				float step = 500.0f * Time.deltaTime;
				f.position = Vector3.MoveTowards(from, to, step);
				f.localScale = new Vector3(1.335679f, 1.335679f, 1.335679f);

				offset += stride / MAX_CARDS; // 10 cards
				count++;
			}
			count = 0;
			offset = 0;
			combinationCount++;
		}
	}

	private bool draggedCardInGraveyardRegion() {
		if(!_player.hasPickedCard()) {
			return false;
		}

		// If the user picked a card from the graveyard, that card must be used in a combination or dropped
		// if(_player.hasPickedFromGraveyard()) {
		// 	if(_draggedCard != _player.getLastCard().getGraphic()) {
		// 		return false;
		// 	}
		// }

		if (_draggedCard) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			float heighthalf = 54f;
			float widthhalf = 40f;

			Vector2 region = graveyard.transform.position;
			if (region.x-widthhalf <= pos.x 
					&& pos.x <= region.x + widthhalf
					&& region.y-heighthalf <= pos.y
					&& pos.y <= region.y+heighthalf)
				return true;
        }

        return false;
	}

	private bool draggedCardInPrivateHandRegion(){
		if (_draggedCard) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			float heighthalf = 44;

			Vector2 region = privateSlotStart.transform.position;
			if ( region.x <= pos.x 
					&& pos.x <= region.x+privateCardsDrawRegionWidth 
					&& region.y-heighthalf <= pos.y
					&& pos.y <= region.y+heighthalf)
				return true;
        }

        return false;
	}

	private bool draggedCardInDropRegion(){
		if (_draggedCard) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			// float heighthalf = 25f;
			// float widthhalf = 18.75f;

			RectTransform r = dropRegionGameobject.GetComponent<RectTransform>();

			return RectTransformUtility.RectangleContainsScreenPoint(r,Input.mousePosition,Camera.main);

			// if ( region.x-widthhalf <= pos.x 
			// 	&& pos.x <= region.x+publicCardsDrawRegionWidth
			// 	&& region.y-heighthalf <= pos.y
			// 	&& pos.y <= region.y+heighthalf)
			// 	return true;

        }

        return false;
	}

	private int draggedCardInPublicHandRegion(){
		if (_draggedCard) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			float heighthalf = 25f;
			float widthhalf = 18.75f;

			for(int i=0 ; i< 4 ; i++){
				Vector2 region = publicSlotStart[i].transform.position;

				if ( region.x-widthhalf <= pos.x 
					&& pos.x <= region.x+publicCardsDrawRegionWidth
					&& region.y-heighthalf <= pos.y
					&& pos.y <= region.y+heighthalf)
					return i;
			}

        }

        return -1;
	}

	private int transformInPublicHandRegion(Transform transform){
		if (transform) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			float heighthalf = 25f;
			float widthhalf = 18.75f;

			for(int i=0 ; i< 4 ; i++){
				Vector2 region = publicSlotStart[i].transform.position;

				if ( region.x-widthhalf <= pos.x 
					&& pos.x <= region.x+publicCardsDrawRegionWidth
					&& region.y-heighthalf <= pos.y
					&& pos.y <= region.y+heighthalf)
					return i;
			}

        }

        return -1;
	}

	// returns the potential position of a dragged card
	// based on the position it's hovering above the other cards
	private int potentialPositionInPrivateHand(Transform draggedCard){
		// int cardsInHands = cardsInHand();
		Hand hand = _player.getHand();
		List<Card> privateCards = hand.getPrivateCards();

		Card prev = null;

		int iter = 1;
		/* go through all cards to determine which card the dragged
			card belongs to */
		for(int i=0;i<privateCards.Count;i++){
			Card c = privateCards[i];

			if (prev == null){
				// is in front of the first card
				if (draggedCard.position.x < c.getGraphic().position.x)
					return iter;
			} else {
				// is between a previous' card X and the current card's x
				if (draggedCard.position.x > prev.getGraphic().position.x && // x > previous card's x
					draggedCard.position.x < c.getGraphic().position.x){ // x < current card's x
					return iter;
				}

				// belongs to last position
				if (draggedCard.position.x > c.getGraphic().position.x && i+1==privateCards.Count){
					return iter;
				}

				// previous location was the one
				if (draggedCard == prev.getGraphic() &&
						draggedCard.position.x < c.getGraphic().position.x){
					return iter-1;
				}

				// dragged card is last card, at last card position
				if (draggedCard.position.x > prev.getGraphic().position.x && i+1==privateCards.Count){
					return iter;
				}
			}

			iter++;
			prev = c;
		}

		return 1;
	}

	private void insertDraggedPrivateCardAtNewPosition(int newPosition){

	}

	private Vector2 getSizeOfCard(Card c){
		Transform texture = c.getGraphic().Find("texture");
		var renderer = texture.GetComponent<Renderer>();
		return new Vector2(renderer.bounds.size.x, renderer.bounds.size.y);
	}

	private int cardsInHand(){
		Hand hand = _player.getHand();
		return hand.getPrivateCards().Count;
	}

	private bool cardIsInPrivateHand(Transform t){
		List<Card> privateCards = _player.getHand().getPrivateCards();

		foreach(Card c in privateCards){
			if (c.getGraphic() == t)
				return true;
		}

		return false;
	}

	private void removeFromPrivatePublicHand(Card c){
		List<Card> privateCards = _player.getHand().getPrivateCards();
		privateCards.Remove(c);

		List<Combination> publicCards = _player.getHand().getPublicCards();
		foreach(Combination comb in publicCards){
			comb._cards.Remove(c);
		}

	}

	private Card getCardFromTransform(Transform t){
		List<Card> privateCards = _player.getHand().getPrivateCards();
		foreach(Card c in privateCards){
			if (c.getGraphic() == t)
				return c;
		}

		List<Combination> publicCards = _player.getHand().getPublicCards();
		foreach(Combination comb in publicCards){
			foreach(Card c in comb.getCards()){
				if (c.getGraphic() == t)
				return c;
			}
		}

		return null;
	}

}