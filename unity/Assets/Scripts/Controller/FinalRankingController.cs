﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class FinalRankingController : MonoBehaviour {
	public GameObject _panel;
	public GameObject[] _rankingNames;
	public GameObject[] _rankingScores;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void showFinalRanking(List<Player> players) {
		hideAll(_rankingScores);
		hideAll(_rankingNames);

		_panel.SetActive(true);

		List<Player> orderedPlayers = new List<Player>(players);
		PlayerScoreComparer comparer = new PlayerScoreComparer();
		orderedPlayers.Sort(comparer);

		for(int i = 0; i < orderedPlayers.Count; i++) {
			Player player = orderedPlayers[orderedPlayers.Count - 1 - i];

			_rankingNames[i].GetComponent<Text>().text = player.getName();
			_rankingScores[i].GetComponent<Text>().text = "" + player.getScore();
			_rankingNames[i].SetActive(true);
			_rankingScores[i].SetActive(true);
		}
	}

	public void newGame() {
		Application.LoadLevel("MainMenu");
	}

	private void hideAll(GameObject[] gameObjectList) {
		foreach(GameObject gameObject in gameObjectList) {
			gameObject.SetActive(false);
		}
	}
}
