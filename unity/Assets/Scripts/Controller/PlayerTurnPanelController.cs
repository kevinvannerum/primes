/*
	Author: Sven Coppers
	Organization: TBA
*/

using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class PlayerTurnPanelController : MonoBehaviour {
	public GameObject _turnPanel;
	public GameObject _newCardButton;
	public GameObject _lastCardButton;
	public GameObject _instruction;

	public GameObject _timer;
	public GameObject _timerBox;

	private Lang _lang;

	/* PLayer Data */
	private float _timeLeft;
	private bool _timerActive;
	private UIPlayer _currentPlayer;
	private bool _combiningAllowed;

	// Use this for initialization
	void Start () {
		_turnPanel.SetActive(false);
		_timerBox.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(_timerActive) {
			_timeLeft -= Time.deltaTime;
			double secondsLeft = Math.Floor(_timeLeft);

			_timer.GetComponent<Text>().text = (int) secondsLeft + _lang.getString("turn_remaining_time");
			_timerBox.SetActive(true);
			_timerBox.GetComponent<ContentSizeFitter>().enabled = true;
			_timer.SetActive(true);

			if(secondsLeft <= 5) {
				_timer.GetComponent<Text>().color = Color.red;

				if(_timeLeft - secondsLeft < 0.5) {
					_timerBox.GetComponent<ContentSizeFitter>().enabled = false;
					_timer.SetActive(false);
				} 
			} else {
				_timer.GetComponent<Text>().color = Color.white;
			}

	        if(_timeLeft <= 0) {
	            _timerActive = false;
	            timeOutAction();
	        }
		}
	}

	public void init(Lang lang) {
		_lang = lang;
	}

	public void showInfoLastCard() {
		_instruction.GetComponent<Text>().text = _lang.getString("last_card");
	}

	public void startTurn(int graveyardSize, bool combiningAllowed) {
		_turnPanel.SetActive(true);
		_newCardButton.SetActive(true);
		_lastCardButton.SetActive(graveyardSize > 0 && combiningAllowed);

		if(graveyardSize > 0 && combiningAllowed) {
			_instruction.GetComponent<Text>().text = _lang.getString("turn_grab_new_old");
		} else {
			_instruction.GetComponent<Text>().text = _lang.getString("turn_grab_new");
		}
		
		_instruction.SetActive(true);
		_combiningAllowed = combiningAllowed;
	}

	public void pickLastCardAction() {
		_newCardButton.SetActive(false);
		_lastCardButton.SetActive(false);
		_currentPlayer.pickLastCard();

		_instruction.GetComponent<Text>().text = _lang.getString("turn_must_combine");
	}

	public void pickNewCardAction() {
		_newCardButton.SetActive(false);
		_lastCardButton.SetActive(false);
		_currentPlayer.pickNewCard();

		if(_combiningAllowed) {
			_instruction.GetComponent<Text>().text = _lang.getString("turn_can_combine");
		} else {
			_instruction.GetComponent<Text>().text = _lang.getString("turn_cannot_combine");
		}
	}

	public void undoLastCardAction() {
		_instruction.SetActive(false);
		_currentPlayer.undoLastCard();

	}

	public void finishTurnAction(Card discardedCard) {
		_turnPanel.SetActive(false);
		_currentPlayer.finishTurn(discardedCard);
	}

	public void timeOutAction() {
		_currentPlayer.timeOut();
	}

	public void startTimer(int remainingTime) {
		_timerActive = true;
		_timeLeft = remainingTime;
		_instruction.SetActive(false);
		_timerBox.SetActive(true);
	}

	public void stopTimer() {
		_timerActive = false;
		_timeLeft = 0;
		_timerBox.SetActive(false);
	}

	public void setCurrentPlayer(UIPlayer currentPlayer) {
		_currentPlayer = currentPlayer;
	}
}
