/**
 * Author: Sven Coppers
 * Organization: TBA
 * 
 * This script is the mainController for the MainMenu Scene
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class PlayerCreator : MonoBehaviour {
	// Range for the amount of players
	public const int MIN_PLAYERS = 2;
	public const int MAX_PLAYERS = 4;

	// GameObjects set by Unity UI
	public GameObject _startButton;
	public GameObject _newPlayerButton;

	public GameObject[] _rows;
	public GameObject[] _humansButtons;
	public GameObject[] _computersEasyButtons;
	public GameObject[] _computersMediumButtons;
	public GameObject[] _computersHardButtons;
	public GameObject[] _removeButtons;

	public Color _humanSelected;
	public Color _humanDeselected;
	public Color _easySelected;
	public Color _easyDeselected;
	public Color _mediumSelected;
	public Color _mediumDeselected;
	public Color _hardSelected;
	public Color _hardDeselected;

	private SharedData _sharedData;
	private SharedData.PlayerType[] _players;
	private int _numPlayers;

	// Use this for initialization
	void Start () {
		_players = new SharedData.PlayerType[4];
		_sharedData = GameObject.Find("SharedData").GetComponent<SharedData>();
		init();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void init() {
		_numPlayers = 2;

		for(int i = 0; i < MAX_PLAYERS; i++) {
			setPlayerUI(i);
		}

		_removeButtons[0].SetActive(_numPlayers == 3);

		for(int i = MIN_PLAYERS; i < MAX_PLAYERS; i++) {
			_rows[i].SetActive(false);
		}
	}

	public void setPlayerEasy(int player) {
		_humansButtons[player].GetComponent<Image>().color = _humanDeselected;
		_computersEasyButtons[player].GetComponent<Image>().color = _easySelected;
		_computersMediumButtons[player].GetComponent<Image>().color = _mediumDeselected;
		_computersHardButtons[player].GetComponent<Image>().color = _hardDeselected;

		_players[player] = SharedData.PlayerType.AI_EASY;
	}

	public void setPlayerMedium(int player) {
		_humansButtons[player].GetComponent<Image>().color = _humanDeselected;
		_computersEasyButtons[player].GetComponent<Image>().color = _easyDeselected;
		_computersMediumButtons[player].GetComponent<Image>().color = _mediumSelected;
		_computersHardButtons[player].GetComponent<Image>().color = _hardDeselected;

		_players[player] = SharedData.PlayerType.AI_MEDIUM;
	}

	public void setPlayerHard(int player) {
		_humansButtons[player].GetComponent<Image>().color = _humanDeselected;
		_computersEasyButtons[player].GetComponent<Image>().color = _easyDeselected;
		_computersMediumButtons[player].GetComponent<Image>().color = _mediumDeselected;
		_computersHardButtons[player].GetComponent<Image>().color = _hardSelected;

		_players[player] = SharedData.PlayerType.AI_HARD;
	}

	public void setPlayerUI(int player) {
		_humansButtons[player].GetComponent<Image>().color = _humanSelected;
		_computersEasyButtons[player].GetComponent<Image>().color = _easyDeselected;
		_computersMediumButtons[player].GetComponent<Image>().color = _mediumDeselected;
		_computersHardButtons[player].GetComponent<Image>().color = _hardDeselected;

		_players[player] = SharedData.PlayerType.UI;
	}

	public void addPlayer() {
		_rows[_numPlayers].SetActive(true);
		_numPlayers = _numPlayers + 1;

		_newPlayerButton.SetActive(_numPlayers < MAX_PLAYERS);
		_removeButtons[0].SetActive(_numPlayers == 3);
	}

	public void removePlayer() {
		_numPlayers = _numPlayers - 1;
		_rows[_numPlayers].SetActive(false);

		_newPlayerButton.SetActive(_numPlayers < MAX_PLAYERS);
		_removeButtons[0].SetActive(_numPlayers == 3);
	}

	public void startGame() {
		List<SharedData.PlayerType> players = new List<SharedData.PlayerType>();

		for(int i = 0; i < _numPlayers; i++) {
			players.Add(_players[i]);
		}

		_sharedData.setPlayers(players);
		Application.LoadLevel("MainGame");
	}
}
