﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class ThirdPersonCardPainter : MonoBehaviour {

	private Player _fpPlayer = null;
	private Player[] _players = null;
	private Graveyard _graveyard = null; // used to draw the upper graveyard card
	private Deck _deck = null;

	public const float MAX_CARDS = 10f;

	public GameObject[] playerStartRegions;
	public GameObject[] playerPlayerInfoRegions;
	public GameObject graveyardPosition;
	public GameObject deckPostion;
	public float cardsHeightOffset = 50;
	public float cardsWidthDistance = 326;

	// Update is called once per frame
	void Update () {
		if (_players == null)
			return;

		drawCards();
	}

	private ThirdPersonCardPainter drawCards(){
		Hand[] hands = getThirdPersonPlayers();
		string[] names = getThirdpersonNames();

		if (hands == null)
			return this;

		drawPublicCards(hands);
		drawPrivateCards(hands,names);
		drawGraveyardCards();
		drawDeckCards();
		return this;
	}

	// Draw the public cards for all 3 TP players
	private void drawPublicCards(Hand[] hands){
		int playerIndex = 0;
		foreach (Hand h in hands){ // hand == player ;D
			GameObject cardsPlayerStart = playerStartRegions[playerIndex];
			List<Combination> publicCards = h.getPublicCards();

			float heightOffset = 0;
			// reposition every combination in the hands
			foreach(Combination comb in publicCards){

				float cardCount = 0;
				float widthOffset = 0;
				foreach (Card c in comb.getCards()){
					// make card visible
					c.setCardVisibility(true);

					// place card on the right position
					Transform f = c.getGraphic();

					// new code
					Vector3 from = f.position;
					Vector3 to = new Vector3(
					 					cardsPlayerStart.transform.position.x + widthOffset,
					 					cardsPlayerStart.transform.position.y - heightOffset,
					 					100 + cardCount	);

					float step = 500.0f * Time.deltaTime;
					f.position = Vector3.MoveTowards(from, to, step);
					f.localScale = new Vector3(0.9f, 0.9f, 1);

					// f.position = new Vector3(
					// 					cardsPlayerStart.transform.position.x + widthOffset,
					// 					cardsPlayerStart.transform.position.y - heightOffset,
					// 					100 + cardCount	);
			

					cardCount+=0.2f;
					widthOffset += cardsWidthDistance / MAX_CARDS;
				}

				heightOffset += cardsHeightOffset;
			}
			
			playerIndex++;
		}
	}

	// Draw priv cards: remove them + update # counter
	private void drawPrivateCards(Hand[] hands, string[] names){
		int playerIndex = 0;
		foreach (Hand h in hands){ // hand == player ;D
			GameObject playerInfoPos = playerPlayerInfoRegions[playerIndex];
			List<Card> privateHand = h.getPrivateCards();

			float slightOffset = 0.0f;
			float slightDepth = 0.0f;

			// reposition every private card
			foreach(Card c in privateHand){
				// shouldnt be visible
				c.setCardVisibility(false);

				// place card on the right position
				// Transform f = c.getGraphic();
				// f.position = new Vector3(
				// 				playerInfoPos.transform.position.x + 13, 
			 // 					playerInfoPos.transform.position.y - 12,
			 // 					150	);

				Transform f = c.getGraphic();
				Vector3 from = f.position;
				Vector3 to = new Vector3(
				 					playerInfoPos.transform.position.x + 15 + slightOffset, 
			 						playerInfoPos.transform.position.y - 12,
			 						150	+ slightDepth );

				float step = 500.0f * Time.deltaTime ;
				f.position = Vector3.MoveTowards(from, to, step);

				f.localScale = new Vector3(0.9f, 0.9f, 1);			
				slightOffset += 1f;
				slightDepth += 0.2f;
			}
			
			// set the text containing number of cards in hand
			playerInfoPos.GetComponent<Text>().text = "x" + privateHand.Count;

			//Transform num = playerInfoPos.transform.Find("text");
		//	num.GetComponent<TextMesh>().text = "x" + privateHand.Count + " - " + names[playerIndex];

			playerIndex++;
		}
	}

	public void drawGraveyardCards(){
		List<Card> cards = _graveyard.getCardStack();
		int depth = 0;
		for(int i=0; i<cards.Count;i++){
			// this card
			Card c = cards[i];
			c.setCardVisibility(false);

			// card position
			Transform f = c.getGraphic();
			f.position = new Vector3(
							graveyardPosition.transform.position.x,
		 					graveyardPosition.transform.position.y,
		 					100	+ depth);

			depth--;

			f.localScale = new Vector3(1.335679f, 1.335679f, 1.335679f);
		}

		// Only the top card should be visible
		if(cards.Count > 0) {
			cards[cards.Count - 1].setCardVisibility(true);
		}
	}

	public void drawDeckCards(){
		List<Card> cards = _deck.getCardStack();
		int depth = 0;
		for(int i=0; i<cards.Count;i++){
			// this card
			Card c = cards[i];
			c.setCardVisibility(false);

			// card position
			Transform f = c.getGraphic();
			f.position = new Vector3(
							deckPostion.transform.position.x,
		 					deckPostion.transform.position.y,
		 					100	+ depth);

			depth--;

			f.localScale = new Vector3(1.335679f, 1.335679f, 1.335679f);
		}
	}

	private Hand[] getThirdPersonPlayers(){
		if (_fpPlayer==null) return null;
		if (_players==null) return null;

		List<Hand> hands = new List<Hand>();
		foreach (Player p in _players){
			if (p == _fpPlayer) // skip if player is first person
				continue;
			hands.Add(p.getHand());
		}

		return hands.ToArray();
	}

	private string[] getThirdpersonNames(){
		if (_fpPlayer==null) return null;
		if (_players==null) return null;

		List<string> names = new List<string>();
		foreach (Player p in _players){
			if (p == _fpPlayer) // skip if player is first person
				continue;
			names.Add(p.getName());
		}

		return names.ToArray();
	}

	public void setCurrentPlayer(Player p){
		_fpPlayer = p;
	}

	public void setReferenceToPlayers(Player[] p){
		_players = p;
	}

	public void setReferences(Graveyard g, Deck d){
		_graveyard = g;
		_deck = d;
	}
}
