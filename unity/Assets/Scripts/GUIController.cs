﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {

	private bool pause = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape)){
			pause = !pause;
		}

	    if(pause){
	       Time.timeScale = 0;
	    }
	    else {
	       Time.timeScale = 1;
	    }
	}

	void OnGUI() {

		 GUIStyle customButton = new GUIStyle("button");
 		customButton.fontSize = Screen.width/30;

		if(pause && GUI.Button(new Rect((Screen.width) /2-Screen.width/8, (Screen.height)/5, Screen.width/4, Screen.width/7), "Main Menu", customButton)){
			Application.LoadLevel("MainMenu");          
		}

		if(pause && GUI.Button(new Rect((Screen.width) /2-Screen.width/8, (Screen.height)/5 + Screen.width/7 + 5, Screen.width/4, Screen.width/7), "Exit Game", customButton)){
			Application.Quit();          
		}

	}

}
