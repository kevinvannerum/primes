using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Model;

public class CardController : MonoBehaviour {
	private Model.Card _card;

	// Use this for initialization
	void Start () {
		// setupGameObject(_card.getColor(), _card.getNumber());
	}
	
	// Update is called once per frame
	void Update () {

	}


	public void setupGameObject(Card.CardColor color, int number, bool isJoker = false){
		// create model card
		_card = new Card(color, number, gameObject.transform, isJoker); // number

		// retrieve the attached 'number' gameobject
		Transform num = gameObject.transform.Find("number");

		// set color of the textmesh
		switch(color){
			case Card.CardColor.Red:
				num.GetComponent<TextMesh>().color = Color.red;
				break;
			case Card.CardColor.Green:
				num.GetComponent<TextMesh>().color = new Color(103f/255,232f/255,86f/255);
				break;
			case Card.CardColor.Blue:
				num.GetComponent<TextMesh>().color = Color.blue;
				break;
			case Card.CardColor.Black:
				num.GetComponent<TextMesh>().color = Color.black;
				break;
			default:
				num.GetComponent<TextMesh>().color = Color.black;
				break;
		}

		// set text
		if (_card.isJoker()){
			num.GetComponent<TextMesh>().color = Color.black;
			num.GetComponent<TextMesh>().text = "J";
		} else {
			num.GetComponent<TextMesh>().text = number+"";
		}
		
	}

	public Card getCard() {
		return _card;
	}
}