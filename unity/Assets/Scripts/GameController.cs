﻿/*
	Author: Adam Blazejczak, Sven Coppers
	Organization: TBA
*/

using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Model;

public class GameController : MonoBehaviour {
	public ScoresController _scoresController;

	public bool _debugging;

	[Range(2, 4)]
	public int _debuggingNumPlayers;

	public GameObject _playField;
	public GameObject _thirdPersonInfo;
	public GameObject _roundPanel;
	public GameObject _roundLabel;
	public GameObject _roundRulesLabel;

	public GameObject _scoresPanel;
	public GameObject _scoresGameLabel;

	public GameObject _rankingPanel;
	public GameObject _rankingGameLabel;

	public GameObject _currentStatePanel;
	public GameObject _currentStatePlayerLabel;
	public GameObject _currentStateRoundLabel;

	public GameObject _passToNextPlayerPanel;
	public GameObject _passToNextPlayerLabel;

	public GameObject _computerBusyPanel;
	public GameObject _computerBusyLabel;
	public GameObject _computerBusyRoundLabel;
	public GameObject _computerBusyGameLabel;

	public GameObject _playerTurnPanel;
	private PlayerTurnPanelController _playerTurnPanelController;
	public GameObject _instruction;

	public GameObject[] _playerPanels;

	public int _startScore = 1000;
	public int _numGames = 3;

	private int _numRounds; // Determined by round_rules.xml

	private int _currentGame = 0;
	private int _currentRound = 0;
	private int _currentPlayer = 0; // Relative to an offset determined by current game
	private string _lastUIPlayer = "";

	public Transform _canvas;
	private RoundRulesLoader _roundRulesLoader;
	private List<Player> _players;
	private List<Card> _cards;
	private Deck _deck;
	private Graveyard _graveyard;

	private FirstPersonCardPainter _fpCardPainter;
	private ThirdPersonCardPainter _tpCardPainter;

	private SharedData _sharedData;
	private Lang _lang;

	// Use this for initialization
	void Start () {
		hideAllUIElements();

		_sharedData = GameObject.Find("SharedData").GetComponent<SharedData>();
		_lang = new Lang("", "English");
		_scoresController.init(_lang);
		// spawnCard(Card.CardColor.Green,44, new Vector2(150,150));

		// Load the controller that will handle all events in the playerTurnPanel
		_playerTurnPanelController = GameObject.Find("PlayerTurnPanelController").GetComponent<PlayerTurnPanelController>();
		_playerTurnPanelController.init(_lang);

		_roundRulesLoader = new RoundRulesLoader();
		_numRounds = _roundRulesLoader.getNumRounds();

		// locate first person card painter
		_fpCardPainter = GameObject.Find("FirstPersonCardPainter").GetComponent<FirstPersonCardPainter>();
		_tpCardPainter = GameObject.Find("ThirdPersonCardPainter").GetComponent<ThirdPersonCardPainter>();

		// Create all cards
		createCards();
		_deck = new Deck();
		_graveyard = new Graveyard();

		// save graveyard yo
		_tpCardPainter.setReferences(_graveyard, _deck);

		if(_debugging) {
			performUnitTests();
		}

		// Play the game
		startGameSequence();

		Time.timeScale = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void hideAllUIElements() {
		//_playField.SetActive(false);
		showPlayFieldCards(false);
		_thirdPersonInfo.SetActive(false);
		_roundPanel.SetActive(false);
		_scoresPanel.SetActive(false);
		_rankingPanel.SetActive(false);
		_currentStatePanel.SetActive(false);
		_passToNextPlayerPanel.SetActive(false);
		_computerBusyPanel.SetActive(false);
		_playerTurnPanel.SetActive(false);
		_instruction.SetActive(false);
	}

	private void initPlayers() {
		_players = new List<Player>();

		if(_sharedData.getPlayers() == null) {
			for(int i = 0; i < _debuggingNumPlayers; i++) {
				_players.Add(new UIPlayer((i + 1), this));
			}

			Debug.Log("GameController Quickstarted by initialialising " + _debuggingNumPlayers + " UI Players");
		} else {
			int computerId = 1;
			int humanId = 1;

			foreach (SharedData.PlayerType p in _sharedData.getPlayers()) {
				switch(p) {
					case SharedData.PlayerType.UI: 
						_players.Add(new UIPlayer(humanId, this));
						humanId++;
						break;
					case SharedData.PlayerType.AI_EASY: 
						_players.Add(new AIPlayer(computerId, this, 3));
						computerId++;
						break;
					case SharedData.PlayerType.AI_MEDIUM: 
						_players.Add(new AIPlayer(computerId, this, 6));
						computerId++;
						break;
					case SharedData.PlayerType.AI_HARD: 
						_players.Add(new AIPlayer(computerId, this, 9));
						computerId++;
						break;
					default:
					  break;
				}
			}
		}

		// Enable other players UI
		for(int i = 0; i < _players.Count - 1; i++) {
			_playerPanels[i].SetActive(true);
		}

		// third person card painter needs to know this
		_tpCardPainter.setReferenceToPlayers(_players.ToArray());
	}

	/**
	 * Start a turn for the current player in the current round
	 * When the player is finished, endTurn will be called
	 * @pre _currentGame is set to the correct game
	 * @pre _currentPlayer is set to the correct player
	 */
	private void initTurn() {
		// Every game the first player is the player after the one who started the previous game
		int playerIndex = (_currentPlayer + _currentGame) % _players.Count;

		if(_players[playerIndex] is AIPlayer) {
			// AI Player
			startTurn();
		} else {
			// If this is the same UI Player
			if(_lastUIPlayer == _players[playerIndex].getName()) {
				startTurn();
			} else {
				// UI Player --> pass the device
				startPassToNextPlayer();
			}
		}
	}

	/**
	 * Start user interaction / AI
	 */
	private void startTurn() {
		Player currentPlayer = getCurrentPlayer();
		updatePlayerLabels();

		if(currentPlayer is UIPlayer) {
			_fpCardPainter.setPlayer((UIPlayer) currentPlayer);
			_tpCardPainter.setCurrentPlayer(currentPlayer);
			_fpCardPainter.setPlayerTurnPanelController(_playerTurnPanelController);
			_lastUIPlayer = currentPlayer.getName();

			_currentStatePanel.SetActive(true);
			_playerTurnPanel.SetActive(true);
			// _playField.SetActive(true);
			showPlayFieldCards(true);
			_instruction.SetActive(true);
			_thirdPersonInfo.SetActive(true);
		} else {
			Hashtable replacements = new Hashtable();
			replacements.Add("computer_name", currentPlayer.getName());
			string text = TemplatingEngine.replace(_lang.getString("computer_turn"), replacements);

			_computerBusyLabel.GetComponent<Text>().text = text;
			_computerBusyPanel.SetActive(true);

			_computerBusyRoundLabel.GetComponent<Text>().text = _lang.getString("round") + " " + (_currentRound + 1);
			_computerBusyGameLabel.GetComponent<Text>().text = _lang.getString("game") + " " + (_currentGame + 1);
		}

		StartCoroutine(currentPlayer.doTurn(_currentRound, _roundRulesLoader.getRoundRules(_currentRound)));
	}

	/* Show or hide all player cards */
	public void showPlayFieldCards(bool show){
		foreach (Transform child in _playField.transform)
    		child.gameObject.SetActive(show);
	}

	/**
	 * This function is called by each kind of player at the end of a turn
	 * @post _currentPlayer is set to the correct player
	 */
	public void endTurn() {
		Player currentPlayer = getCurrentPlayer();

		if(currentPlayer is UIPlayer) {
			// Discard invalid combinations made during turn
			_fpCardPainter.endTurnProcedures();
			_playerTurnPanelController.stopTimer();
		}

		hideAllUIElements();

		// If this was the last player this round, 
		if(_currentPlayer < _players.Count - 1) {
			_currentPlayer++;
			initTurn();
		} else {
			// The round has ended
			endRound();
		}
	}

	/**
	 * Start the round with the _currentRound, each player should perform a turn
	 * @pre _currentRound is set to the correct round
	 * @post _currentPlayer is set to the correct player
	 */
	private void startRound() {
		_currentStateRoundLabel.GetComponent<Text>().text = "" + (_currentRound + 1);
		_fpCardPainter.updateRoundRules(_roundRulesLoader.getRoundRules(_currentRound));
		_currentPlayer = 0;
		initTurn();
	}

	/**
	 * Show an overview of the rules for the current round
	 */
	private void startRoundRules() {
		_roundPanel.SetActive(true);
		_roundLabel.GetComponent<Text>().text = _lang.getString("round") + " " + (_currentRound + 1);
		_roundRulesLabel.GetComponent<Text>().text = _roundRulesLoader.getRoundRules(_currentRound).toString(_lang);
	}

	/**
	 * Stop showing an overview of the rules for the current round
	 */
	public void endRoundRules() {
		_roundPanel.SetActive(false);
		startRound();
	}

	/**
	 * Ask the player to pass the device to next player
	 */
	private void startPassToNextPlayer() {
		Hashtable replacements = new Hashtable();
		replacements.Add("next_player", getCurrentPlayer().getName());
		string text = TemplatingEngine.replace(_lang.getString("turn_upcoming_player"), replacements);
		_passToNextPlayerLabel.GetComponent<Text>().text = text;
		_passToNextPlayerPanel.SetActive(true);
	}

	/**
	 * Resume the game after the current player confirmed he/she has the device
	 */
	public void endPassToNextPlayer() {
		_passToNextPlayerPanel.SetActive(false);
		startTurn();
	}

	/**
	 * This function is called when every player performed it's turn
	 * @post _currentRound is set to the correct round
	 */
	private void endRound() {
		// If this was the last round in this game, the game has ended
		if(_currentRound < _numRounds - 1) {
			// Play another round
			_currentRound++;
			//startRoundRules();
			startRound();
		} else {
			// The game has ended
			startBestCombinations();
		}
	}

	private void startBestCombinations() {
		Hashtable replacements = new Hashtable();
		replacements.Add("number", "" + (_currentGame + 1));
		string gameLabel = TemplatingEngine.replace(_lang.getString("game_number"), replacements);


		_scoresController.showBestCombinations();
		_scoresGameLabel.GetComponent<Text>().text = gameLabel;
		_scoresPanel.SetActive(true);
	}

	public void endBestCombinations() {
		_scoresPanel.SetActive(false);
		startRanking();
	}

	private void startRanking() {
		Hashtable replacements = new Hashtable();
		replacements.Add("number", "" + (_currentGame + 1));
		string gameLabel = TemplatingEngine.replace(_lang.getString("game_number"), replacements);
		
		_scoresController.updateScores();
		_rankingGameLabel.GetComponent<Text>().text = gameLabel;
		_rankingPanel.SetActive(true);
	}

	public void endRanking() {
		_rankingPanel.SetActive(false);
		endGame();
	}

	/**
	 * Start a game with the _currentGame, every round should be executed
	 * @pre _currentGame is set to the correct game
	 */
	private void startGame() {
		// Reset Cards
		resetCards();

		for(int i = 0; i < _players.Count; i++) {
			_players[i].newGame();
		}

		_currentRound = 0;

		// Geef iedere speler 10 kaarten
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < _players.Count; j++) {
				_players[j].getHand().addCardToPrivate(_deck.popCard());
			}
		}

		// make 4 combination placeholders to each player
		for(int i = 0; i < _players.Count; i++ ) {
			for(int j = 0; j < 4; j++)
				if (_players[i].isAI() == false)
					_players[i].getHand().addCombinationToPublic( new Combination() );
		}

		if(_debugging) {
			addTestCombinations();
		}

		//startRoundRules();
		startRound();
	}

	private void endGame() {
		// TODO: Update and show scores

		// TODO: Keep playing games until?
		// 		[ ] One player has -1001 or 0 points?
		// 		[ ] The player wants to stop?
		//		[X] after 3 games for debugging purposes
		//		[ ] after 10 games (see Priemen+.doc)

		if(_currentGame < _numGames - 1) {
			// Play another game
			_currentGame++;
			startGame();
		} else {
			endGameSequence();
		}
	}

	/**
	 * Play multiple games one after another. 
	 */
	private void startGameSequence() {
		// Init players
		initPlayers();
		_scoresController.setPlayers(_players);

		_currentGame = 0;
		startGame();
	}

	private void endGameSequence() {
		GetComponent<FinalRankingController>().showFinalRanking(_players);
	}

	public void resetCards() {
		_deck.clear();
		_graveyard.clear();

		for(int i = 0; i < _cards.Count; i++) {
			_deck.addCard(_cards[i]);
		}

		_deck.shuffle();

		/* reset positions */
		ThirdPersonCardPainter g = GameObject.Find("ThirdPersonCardPainter").GetComponent<ThirdPersonCardPainter>();
		g.drawGraveyardCards();
		g.drawDeckCards();
	}

	/**
	 * Every game the first player is the player after the one who started the previous game
	 */
	private Player getCurrentPlayer() {
		return _players[getCurrentPlayerIndex()];
	}

	private int getCurrentPlayerIndex() {
		return (_currentPlayer + _currentGame) % _players.Count;
	}

	public RoundRulesLoader getRoundRulesLoader() {
		return _roundRulesLoader;
	}

	public PlayerTurnPanelController getPlayerTurnPanelController() {
		return _playerTurnPanelController;
	}

	public Deck getDeck() {
		return _deck;
	}

	public Graveyard getGraveyard() {
		return _graveyard;
	}

	public int getStartScore() {
		return _startScore;
	}

	private void updatePlayerLabels() {
		int currentPlayerIndex = getCurrentPlayerIndex();
		bool currentPlayerFound = false;

		for(int i = 0; i < _players.Count; i++) {
			if(i == currentPlayerIndex) {
				_currentStatePlayerLabel.GetComponent<Text>().text = _players[currentPlayerIndex].getName();
				currentPlayerFound = true;
			} else {
				int playerPanelIndex = i;

				if(currentPlayerFound) {
					playerPanelIndex -= 1;
				}

				Transform f = _playerPanels[playerPanelIndex].transform.Find("Player" + (playerPanelIndex + 1) + "Name");
				f.GetComponent<Text>().text = _players[i].getName();
				
				Transform p = _playerPanels[playerPanelIndex].transform.Find("Player" + (playerPanelIndex + 1) + "NameNumCards");
				p.GetComponent<Text>().text = "x" + _players[i].getHand().getPrivateCards().Count;
			}
		}
	}

	/**
	 * Creates all the cards and puts them in a List, but not yet to a deck
	 */	
	private void createCards() {
		_cards = new List<Card>();
		CardFactory cf = GameObject.Find("GameController").GetComponent<CardFactory>();
		List<int> primes = CombinationChecker.generatePrimes();

		foreach(int prime in primes) {
			_cards.Add(cf.createCard(Card.CardColor.Red, prime));
			_cards.Add(cf.createCard(Card.CardColor.Green, prime));
			_cards.Add(cf.createCard(Card.CardColor.Blue, prime));
		}

		_cards.Add(cf.createCard(true));
	}

	private void performUnitTests() {
		// Testers should be Scriptable objects in order to enable logging and can therefore not be initiated using "new"
		CombinationCheckerTester cct = ScriptableObject.CreateInstance("CombinationCheckerTester") as CombinationCheckerTester;
		cct.runTests();
	}

	private void addTestCombinations() {
		CardFactory cf = GameObject.Find("GameController").GetComponent<CardFactory>();

		Combination combination1 = new Combination();
		combination1.addCard(cf.createCard(Card.CardColor.Red, 23));
		combination1.addCard(cf.createCard(Card.CardColor.Blue, 31));
		combination1.addCard(cf.createCard(Card.CardColor.Green, 47));
		combination1.addCard(cf.createCard(Card.CardColor.Blue, 71));
		_players[0].getHand().addCombinationToPublic(combination1);

		Combination combination2 = new Combination();
		combination2.addCard(cf.createCard(Card.CardColor.Blue, 11));
		combination2.addCard(cf.createCard(Card.CardColor.Green, 13));
		combination2.addCard(cf.createCard(Card.CardColor.Blue, 17));
		_players[1].getHand().addCombinationToPublic(combination2);

		Combination combination3 = new Combination();
		combination3.addCard(cf.createCard(Card.CardColor.Blue, 17));
		combination3.addCard(cf.createCard(Card.CardColor.Red, 19));
		combination3.addCard(cf.createCard(Card.CardColor.Blue, 23));
		combination3.addCard(cf.createCard(Card.CardColor.Green, 29));
		combination3.addCard(cf.createCard(Card.CardColor.Green, 37));
		combination3.addCard(cf.createCard(Card.CardColor.Blue, 47));
		combination3.addCard(cf.createCard(Card.CardColor.Red, 59));
		combination3.addCard(cf.createCard(Card.CardColor.Blue, 73));
		_players[1].getHand().addCombinationToPublic(combination3);

		if(_players.Count > 2) {
			Combination combination4 = new Combination();
			combination4.addCard(cf.createCard(Card.CardColor.Blue, 17));
			combination4.addCard(cf.createCard(Card.CardColor.Red, 19));
			combination4.addCard(cf.createCard(Card.CardColor.Blue, 23));
			combination4.addCard(cf.createCard(Card.CardColor.Green, 29));
			combination4.addCard(cf.createCard(Card.CardColor.Green, 37));
			combination4.addCard(cf.createCard(Card.CardColor.Blue, 47));
			combination4.addCard(cf.createCard(Card.CardColor.Red, 59));
			combination4.addCard(cf.createCard(Card.CardColor.Red, 73));
			_players[2].getHand().addCombinationToPublic(combination4);
		}

		if(_players.Count > 3) {
			Combination combination5 = new Combination();
			combination5.addCard(cf.createCard(Card.CardColor.Green, 23));
			combination5.addCard(cf.createCard(Card.CardColor.Green, 31));
			combination5.addCard(cf.createCard(Card.CardColor.Green, 47));
			combination5.addCard(cf.createCard(Card.CardColor.Green, 71));
			_players[3].getHand().addCombinationToPublic(combination5);
		}
	}
}